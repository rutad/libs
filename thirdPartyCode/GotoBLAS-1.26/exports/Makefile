TOPDIR	= ..

include ../Makefile.rule
include ../level3/gemm/$(ARCHSUBDIR)/Makefile

HPLS  = \
	memory.$(SUFFIX) xerbla.$(SUFFIX) parameter.$(SUFFIX) \
	daxpy_k.$(SUFFIX) daxpy.$(SUFFIX) dcopy_k.$(SUFFIX) dcopy.$(SUFFIX) ddot_k.$(SUFFIX) ddot.$(SUFFIX) \
	dger_k.$(SUFFIX) dger.$(SUFFIX) dscal_k.$(SUFFIX) dscal.$(SUFFIX) \
	idamax_k.$(SUFFIX) idamax.$(SUFFIX) \
	dtrsv.$(SUFFIX) \
	dtrsv_NLU.$(SUFFIX) dtrsv_NUN.$(SUFFIX) dtrsv_NUU.$(SUFFIX) dtrsv_NLN.$(SUFFIX) \
	dtrsv_TLN.$(SUFFIX) dtrsv_TLU.$(SUFFIX) dtrsv_TUN.$(SUFFIX) dtrsv_TUU.$(SUFFIX) \
	dgemv.$(SUFFIX) dgemv_n.$(SUFFIX) dgemv_t.$(SUFFIX) \
	dgemm.$(SUFFIX) dgemm_kernel.$(SUFFIX) \
	dgemm_nn.$(SUFFIX) dgemm_nt.$(SUFFIX) dgemm_tn.$(SUFFIX) dgemm_tt.$(SUFFIX) \
	$(DGEMMINCOPYOBJ) $(DGEMMITCOPYOBJ) $(DGEMMONCOPYOBJ) $(DGEMMOTCOPYOBJ) \
	dgemm_beta.$(SUFFIX) \
	dtrsm.$(SUFFIX) \
	dtrsm_kernel_LN.$(SUFFIX) dtrsm_kernel_LT.$(SUFFIX) \
	dtrsm_kernel_RN.$(SUFFIX) dtrsm_kernel_RT.$(SUFFIX) \
	dtrsm_LNUU.$(SUFFIX) dtrsm_LNUN.$(SUFFIX) dtrsm_LNLU.$(SUFFIX) dtrsm_LNLN.$(SUFFIX) \
	dtrsm_LTUU.$(SUFFIX) dtrsm_LTUN.$(SUFFIX) dtrsm_LTLU.$(SUFFIX) dtrsm_LTLN.$(SUFFIX) \
	dtrsm_RNUU.$(SUFFIX) dtrsm_RNUN.$(SUFFIX) dtrsm_RNLU.$(SUFFIX) dtrsm_RNLN.$(SUFFIX) \
	dtrsm_RTUU.$(SUFFIX) dtrsm_RTUN.$(SUFFIX) dtrsm_RTLU.$(SUFFIX) dtrsm_RTLN.$(SUFFIX) \
	dtrsm_iunucopy.$(SUFFIX) dtrsm_iunncopy.$(SUFFIX) \
	dtrsm_ilnucopy.$(SUFFIX) dtrsm_ilnncopy.$(SUFFIX) \
	dtrsm_iutucopy.$(SUFFIX) dtrsm_iutncopy.$(SUFFIX) \
	dtrsm_iltucopy.$(SUFFIX) dtrsm_iltncopy.$(SUFFIX) \
	dtrsm_ounucopy.$(SUFFIX) dtrsm_ounncopy.$(SUFFIX) \
	dtrsm_olnucopy.$(SUFFIX) dtrsm_olnncopy.$(SUFFIX) \
	dtrsm_outucopy.$(SUFFIX) dtrsm_outncopy.$(SUFFIX) \
	dtrsm_oltucopy.$(SUFFIX) dtrsm_oltncopy.$(SUFFIX) \
	HPL_dlaswp00N.$(SUFFIX)  dlaswp00N.$(SUFFIX) \
	HPL_dlaswp01N.$(SUFFIX)  dlaswp01N.$(SUFFIX) \
	HPL_dlaswp01T.$(SUFFIX)  dlaswp01T.$(SUFFIX)


ifdef SMP
HPLS += blasL1thread.$(SUFFIX) blas_server.$(SUFFIX) gemm_thread_variable.$(SUFFIX) \
	gemm_thread_m.$(SUFFIX) gemm_thread_mn.$(SUFFIX) gemm_thread_n.$(SUFFIX) \
	gemv_thread.$(SUFFIX) ger_thread.$(SUFFIX) divtable.$(SUFFIX)

ifndef USE_SIMPLE_THREADED_LEVEL3
HPLS += dgemm_thread_nn.$(SUFFIX) dgemm_thread_nt.$(SUFFIX) dgemm_thread_tn.$(SUFFIX) dgemm_thread_tt.$(SUFFIX)
endif
endif


ifdef STATIC_ALLOCATION
HPLS	+= staticbuffer.$(SUFFIX)
endif

HPLS_P = $(HPLS:.$(SUFFIX)=.$(PSUFFIX))

ifndef EXPRECISION
EXPRECISION	= 0
endif

all:

libs:

prof:

hpl: libgoto_hpl.$(LIBSUFFIX)

hpl_p: libgoto_hpl_p.$(LIBSUFFIX)

$(HPLS) : ../$(LIBNAME)
	$(AR) x ../$(LIBNAME) $(HPLS)

$(HPLS_P) : ../$(LIBNAME_P)
	$(AR) x ../$(LIBNAME_P)  $(HPLS_P)

libgoto_hpl.$(LIBSUFFIX) : $(HPLS)
	rm -f libgoto_hpl.$(LIBSUFFIX)
	$(LD) -r $(LDFLAGS) -o goto.$(SUFFIX) $(HPLS)
	$(AR) cq libgoto_hpl.$(LIBSUFFIX) goto.$(SUFFIX)
	$(RANLIB) libgoto_hpl.$(LIBSUFFIX)
	rm -f *.$(SUFFIX)

libgoto_hpl_p.$(LIBSUFFIX) : $(HPLS_P)
	rm -f libgoto_hpl_p.$(LIBSUFFIX)
	$(LD) -r  $(LDFLAGS) -o goto.$(SUFFIX) $(HPLS_P)
	$(AR) cq libgoto_hpl_p.$(LIBSUFFIX) goto.$(SUFFIX)
	$(RANLIB) libgoto_hpl_p.$(LIBSUFFIX)
	rm -f *.$(PSUFFIX)

dyn : $(LIBDYNNAME)

zip : dll
	zip $(LIBZIPNAME) $(LIBDLLNAME) $(LIBWIN2KNAME)

dll : $(LIBDLLNAME)

DLLINIT      = dllinit@12

ifneq ($(C_COMPILER), PGI)

$(LIBDLLNAME) : ../$(LIBNAME) $(LIBDEFNAME) dllinit.$(SUFFIX)
	$(PREFIX)dllwrap -mno-cygwin -o $(LIBDLLNAME) --def $(LIBDEFNAME) \
	--entry _$(DLLINIT) -s \
	dllinit.$(SUFFIX) ../$(LIBNAME)
	lib /machine:i386 /def:$(LIBDEFNAME)

$(LIBDEFNAME) : gensymbol
	./gensymbol win2k dummy  $(EXPRECISION) $(ARCH) > $(LIBDEFNAME)

else

$(LIBDLLNAME) : ../$(LIBNAME) $(LIBDEFNAME)
	pgcc -Mmakedll -o $(LIBDLLNAME) -def:$(LIBDEFNAME) ../$(LIBNAME)

$(LIBDEFNAME) : gensymbol
	./gensymbol microsoft dummy  $(EXPRECISION) $(ARCH) > $(LIBDEFNAME)

endif


$(LIBDYNNAME) : ../$(LIBNAME) osx.def
	$(PREFIX)gcc $(CFLAGS) -all_load -dynamiclib -o $(LIBDYNNAME) $< -Wl,-exported_symbols_list,osx.def

symbol.$(SUFFIX) : symbol.S
	$(CC) $(CFLAGS) -c -o $(@F) $^

dllinit.$(SUFFIX) : dllinit.S
	$(CC) $(CFLAGS) -c -o $(@F) -s $<

ifeq ($(OSNAME), Linux)

so : ../$(LIBSONAME) test

../$(LIBSONAME) : ../$(LIBNAME) linux.def
	$(CC) $(CFLAGS) -shared -o ../$(LIBSONAME) \
	-Wl,--whole-archive ../$(LIBNAME) -Wl,--no-whole-archive \
	-Wl,--retain-symbols-file=linux.def $(EXTRALIB)

endif

ifeq ($(OSNAME), FreeBSD)

so : ../$(LIBSONAME) test

../$(LIBSONAME) : ../$(LIBNAME) linux.def
	$(CC) $(CFLAGS) -shared -o ../$(LIBSONAME) \
	-Wl,--whole-archive ../$(LIBNAME) -Wl,--no-whole-archive \
	-Wl,--retain-symbols-file=linux.def

endif

ifeq ($(OSNAME), OSF1)

so : ../$(LIBSONAME)

../$(LIBSONAME) :
	$(CC) $(CFLAGS) -shared -o ../$(LIBSONAME) ../$(LIBNAME)
endif

ifeq ($(OSNAME), SunOS)

so : ../$(LIBSONAME)

../$(LIBSONAME) : linux.def
	$(LD) $(LDFLAGS) -G -z allextract -o ../$(LIBSONAME) ../$(LIBNAME)

endif

ifeq ($(OSNAME), AIX)

ifeq ($(COMPILER_F77), xlf)

goto32.$(SUFFIX) : ../$(LIBNAME) aix.def
	ld -o $(@F) ../$(LIBNAME) -bE:aix.def -bM:SRE -bnoexpall -bnoentry -L$(HOME)/misc/lib -lxlf90 -lc -lm  -lpthread

goto64.$(SUFFIX) : ../$(LIBNAME) aix.def
	ld -b64 -o $(@F) ../$(LIBNAME) -bE:aix.def -bM:SRE -bnoexpall -bnoentry -L$(HOME)/misc/lib/ppc64 -lxlf90 -lc -lm -lpthread
else
goto32.$(SUFFIX) : ../$(LIBNAME) aix.def
	ld -o $(@F) ../$(LIBNAME) -bE:aix.def -bM:SRE -bnoexpall -bnoentry -L$(HOME)/misc/lib -lg2c -lc -lm 

goto64.$(SUFFIX) : ../$(LIBNAME) aix.def
	ld -b64 -o $(@F) ../$(LIBNAME) -bE:aix.def -bM:SRE -bnoexpall -bnoentry -L$(HOME)/misc/lib/ppc64 -lg2c -lc -lm 
endif
endif

static : ../$(LIBNAME)
	$(LD) $(LDFLAGS) -r -o goto.$(SUFFIX) \
	--whole-archive ../$(LIBNAME) --no-whole-archive
	rm -f ../$(LIBNAME)
	$(AR) -cq ../$(LIBNAME) goto.$(SUFFIX)
	rm -f goto.$(SUFFIX)

linux.def : gensymbol ../Makefile.rule ../getarch.c
	./gensymbol linux $(BU) $(EXPRECISION) $(ARCH) > $(@F)

osx.def : gensymbol ../Makefile.rule ../getarch.c
	./gensymbol osx $(BU) $(EXPRECISION) $(ARCH) > $(@F)

aix.def : gensymbol ../Makefile.rule ../getarch.c
	./gensymbol aix $(BU) $(EXPRECISION) $(ARCH) > $(@F)

symbol.S : gensymbol
	./gensymbol win2kasm dummy  $(EXPRECISION) > symbol.S

test : linktest.c
	$(CC) $(CFLAGS) -w -o linktest linktest.c ../$(LIBSONAME) $(EXTRALIB) -lm && echo OK.
	rm -f linktest

linktest.c : gensymbol ../Makefile.rule ../getarch.c
	./gensymbol linktest $(BU)  $(EXPRECISION) > linktest.c

clean ::
	@rm -f *.def *.dylib __.SYMDEF*

include ../Makefile.tail


