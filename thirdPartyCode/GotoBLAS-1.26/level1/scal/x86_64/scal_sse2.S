/*********************************************************************/
/*                                                                   */
/*             Optimized BLAS libraries                              */
/*                     By Kazushige Goto <kgoto@tacc.utexas.edu>     */
/*                                                                   */
/* Copyright (c) The University of Texas, 2005. All rights reserved. */
/* UNIVERSITY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES CONCERNING  */
/* THIS SOFTWARE AND DOCUMENTATION, INCLUDING ANY WARRANTIES OF      */
/* MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE,              */
/* NON-INFRINGEMENT AND WARRANTIES OF PERFORMANCE, AND ANY WARRANTY  */
/* THAT MIGHT OTHERWISE ARISE FROM COURSE OF DEALING OR USAGE OF     */
/* TRADE. NO WARRANTY IS EITHER EXPRESS OR IMPLIED WITH RESPECT TO   */
/* THE USE OF THE SOFTWARE OR DOCUMENTATION.                         */
/* Under no circumstances shall University be liable for incidental, */
/* special, indirect, direct or consequential damages or loss of     */
/* profits, interruption of business, or related expenses which may  */
/* arise from use of Software or Documentation, including but not    */
/* limited to those resulting from defects in Software and/or        */
/* Documentation, or loss or inaccuracy of data of any kind.         */
/*********************************************************************/

#define ASSEMBLER
#include "common.h"

#define PREFETCH_SIZE 96

#ifndef WINDOWS_ABI
#define M	ARG1
#define X	ARG4
#define INCX	ARG5
#else
#define M	ARG1
#define X	ARG2
#define INCX	ARG3
#endif

#define XX	%r10
#define I	%rax

	PROLOGUE
	PROFCODE

#ifdef WINDOWS_ABI
	movq	40(%rsp), X
	movq	48(%rsp), INCX

	movaps	%xmm3, %xmm0	
#endif

	SAVEREGISTERS
	
	testq	M, M
	jle	.L999

	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	lea	(, INCX, SIZE), INCX
	jne	.L100		# Alpha != ZERO

/* Alpha == ZERO */
	cmpq	$SIZE, INCX
	jne	.L50

/* INCX == 1 */
	testq	$15, X		# aligned for quad word?
	je	.L05

	movsd	%xmm1, 0 * SIZE(X)
	addq	$SIZE, X
	decq	M
	jle	.L999
	ALIGN_3
.L05:

/* Aligned Mode */
	movq	M,  I	# rcx = n
	sarq	$4, I
	jle	.L12
	ALIGN_4

.L11:
#ifdef HAVE_3DNOW
	prefetchw	(PREFETCH_SIZE + 0) * SIZE(X)
#endif
	movapd	%xmm1,  0 * SIZE(X)
	movapd	%xmm1,  2 * SIZE(X)
	movapd	%xmm1,  4 * SIZE(X)
	movapd	%xmm1,  6 * SIZE(X)

#ifdef HAVE_3DNOW
	prefetchw	(PREFETCH_SIZE + 8) * SIZE(X)
#endif
	movapd	%xmm1,  8 * SIZE(X)
	movapd	%xmm1, 10 * SIZE(X)
	movapd	%xmm1, 12 * SIZE(X)
	movapd	%xmm1, 14 * SIZE(X)

	addq	$16 * SIZE, X
	decq	I
	jg	.L11
	ALIGN_4

.L12:
	testq	$15, M
	je	.L999
	testq	$8, M
	je	.L13

	movapd	%xmm1,  0 * SIZE(X)
	movapd	%xmm1,  2 * SIZE(X)
	movapd	%xmm1,  4 * SIZE(X)
	movapd	%xmm1,  6 * SIZE(X)
	addq	$8 * SIZE, X
	ALIGN_3

.L13:
	testq  $4, M
	je    .L14

	movapd	%xmm1,  0 * SIZE(X)
	movapd	%xmm1,  2 * SIZE(X)
	addq	$4 * SIZE, X
	ALIGN_3

.L14:
	testq  $2, M
	je    .L15

	movapd	%xmm1,  0 * SIZE(X)
	addq	$2 * SIZE, X
	ALIGN_3

.L15:
	testq  $1, M
	je    .L999

	movsd	%xmm1,  0 * SIZE(X)
	jmp	.L999
	ALIGN_4


/* incx != 1 */
.L50:
	movq	M,  I		# rcx = n
	sarq	$3, I		# (n >> 3)
	jle	.L52
	ALIGN_4

.L51:
#ifdef HAVE_3DNOW
	prefetchw	(PREFETCH_SIZE + 0) * SIZE(X)
#endif
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X

	decq	I
	jg	.L51
	ALIGN_4

.L52:
	testq	$7, M
	je	.L999

	testq	$4, M
	je	.L53

	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	ALIGN_3

.L53:
	testq	$2, M
	je	.L54

	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	movsd	%xmm1, 0 * SIZE(X)
	addq	INCX, X
	ALIGN_3

.L54:
	testq	$1, M
	je	.L999

	movsd	%xmm1, 0 * SIZE(X)
	jmp	.L999
	ALIGN_4

/* Alpha != ZERO */

.L100:
	cmpq	$SIZE, INCX
	jne	.L150

	testq	$15, X		# aligned for quad word?
	je	.L105

	movsd	0 * SIZE(X), %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 0 * SIZE(X)
	addq	$SIZE, X
	decq	M
	jle	.L999
	ALIGN_3
.L105:

/* Aligned Mode */
.L110:
	unpcklpd %xmm0, %xmm0

	movq	M,  I		# rcx = n
	sarq	$4, I
	jle	.L113

	movapd	 0 * SIZE(X), %xmm1
	movapd	 2 * SIZE(X), %xmm2
	movapd	 4 * SIZE(X), %xmm3
	movapd	 6 * SIZE(X), %xmm4
	movapd	 8 * SIZE(X), %xmm5
	movapd	10 * SIZE(X), %xmm6
	movapd	12 * SIZE(X), %xmm7
	movapd	14 * SIZE(X), %xmm8

	decq	I 
	jle	.L112
	ALIGN_4

.L111:
#ifdef HAVE_3DNOW
	prefetchw	(PREFETCH_SIZE + 0) * SIZE(X)
#endif
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm3
	mulpd	%xmm0, %xmm4

	movapd	%xmm1,  0 * SIZE(X)
	movapd	%xmm2,  2 * SIZE(X)
	movapd	%xmm3,  4 * SIZE(X)
	movapd	%xmm4,  6 * SIZE(X)

	mulpd	%xmm0, %xmm5
	mulpd	%xmm0, %xmm6
	mulpd	%xmm0, %xmm7
	mulpd	%xmm0, %xmm8

#ifdef HAVE_3DNOW
	prefetchw	(PREFETCH_SIZE + 8) * SIZE(X)
#endif
	movapd	%xmm5,  8 * SIZE(X)
	movapd	%xmm6, 10 * SIZE(X)
	movapd	%xmm7, 12 * SIZE(X)
	movapd	%xmm8, 14 * SIZE(X)

	movapd	16 * SIZE(X), %xmm1
	movapd	18 * SIZE(X), %xmm2
	movapd	20 * SIZE(X), %xmm3
	movapd	22 * SIZE(X), %xmm4

	movapd	24 * SIZE(X), %xmm5
	movapd	26 * SIZE(X), %xmm6
	movapd	28 * SIZE(X), %xmm7
	movapd	30 * SIZE(X), %xmm8

	addq	$16 * SIZE, X
	decq	I
	jg	.L111
	ALIGN_4

.L112:
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm3
	mulpd	%xmm0, %xmm4

	movapd	%xmm1,  0 * SIZE(X)
	movapd	%xmm2,  2 * SIZE(X)
	movapd	%xmm3,  4 * SIZE(X)
	movapd	%xmm4,  6 * SIZE(X)

	mulpd	%xmm0, %xmm5
	mulpd	%xmm0, %xmm6
	mulpd	%xmm0, %xmm7
	mulpd	%xmm0, %xmm8

	movapd	%xmm5,  8 * SIZE(X)
	movapd	%xmm6, 10 * SIZE(X)
	movapd	%xmm7, 12 * SIZE(X)
	movapd	%xmm8, 14 * SIZE(X)
	addq	$16 * SIZE, X
	ALIGN_3

.L113:
	testq	$15, M
	je	.L999

	testq	$8, M
	je	.L114

	movapd	 0 * SIZE(X), %xmm1
	movapd	 2 * SIZE(X), %xmm2
	movapd	 4 * SIZE(X), %xmm3
	movapd	 6 * SIZE(X), %xmm4
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	mulpd	%xmm0, %xmm3
	mulpd	%xmm0, %xmm4
	movapd	%xmm1,  0 * SIZE(X)
	movapd	%xmm2,  2 * SIZE(X)
	movapd	%xmm3,  4 * SIZE(X)
	movapd	%xmm4,  6 * SIZE(X)
	addq	$8 * SIZE, X
	ALIGN_3

.L114:
	testq	$4, M
	je	.L115

	movapd	 0 * SIZE(X), %xmm1
	movapd	 2 * SIZE(X), %xmm2
	mulpd	%xmm0, %xmm1
	mulpd	%xmm0, %xmm2
	movapd	%xmm1,  0 * SIZE(X)
	movapd	%xmm2,  2 * SIZE(X)
	addq	$4 * SIZE, X
	ALIGN_3

.L115:
	testq	$2, M
	je	.L116

	movapd	 0 * SIZE(X), %xmm1
	mulpd	%xmm0, %xmm1
	movapd	%xmm1,  0 * SIZE(X)
	addq	$2 * SIZE, X
	ALIGN_3

.L116:
	testq	$1, M
	je	.L999

	movsd	 0 * SIZE(X), %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1,  0 * SIZE(X)
	jmp	.L999
	ALIGN_3

/* incx != 1 */

.L150:
	movq	X, XX
	movq	M,  I		# rcx = n
	sarq	$3, I		# (n >> 3)
	jle	.L152
	ALIGN_4

.L151:
#ifdef HAVE_3DNOW
	prefetchw	(PREFETCH_SIZE + 0) * SIZE(X)
#endif
	movsd	0 * SIZE(X), %xmm1
	addq	INCX, X
	movsd	0 * SIZE(X), %xmm2
	addq	INCX, X
	movsd	0 * SIZE(X), %xmm3
	addq	INCX, X
	movsd	0 * SIZE(X), %xmm4
	addq	INCX, X
	movsd	0 * SIZE(X), %xmm5
	addq	INCX, X
	movsd	0 * SIZE(X), %xmm6
	addq	INCX, X
	movsd	0 * SIZE(X), %xmm7
	addq	INCX, X
	movsd	0 * SIZE(X), %xmm8
	addq	INCX, X

	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm5
	mulsd	%xmm0, %xmm6
	mulsd	%xmm0, %xmm7
	mulsd	%xmm0, %xmm8

	movsd	%xmm1, 0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm2, 0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm3, 0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm4, 0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm5, 0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm6, 0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm7, 0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm8, 0 * SIZE(XX)
	addq	INCX, XX
	decq	I
	jg	.L151
	ALIGN_4

.L152:
	testq	$7, M
	je	.L999

	testq	$4, M
	je	.L153

	movsd	 0 * SIZE(X), %xmm1
	addq	INCX, X
	movsd	 0 * SIZE(X), %xmm2
	addq	INCX, X
	movsd	 0 * SIZE(X), %xmm3
	addq	INCX, X
	movsd	 0 * SIZE(X), %xmm4
	addq	INCX, X

	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm4

	movsd	%xmm1,  0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm2,  0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm3,  0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm4,  0 * SIZE(XX)
	addq	INCX, XX
	ALIGN_3

.L153:
	testq	$2, M
	je	.L154

	movsd	 0 * SIZE(X), %xmm1
	addq	INCX, X
	movsd	 0 * SIZE(X), %xmm2
	addq	INCX, X

	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm2

	movsd	%xmm1,  0 * SIZE(XX)
	addq	INCX, XX
	movsd	%xmm2,  0 * SIZE(XX)
	addq	INCX, XX
	ALIGN_3

.L154:
	testq	$1, M
	je	.L999

	movsd	 0 * SIZE(X), %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1,  0 * SIZE(X)
	ALIGN_4

.L999:
	xorq	%rax, %rax

	RESTOREREGISTERS

	ret

	EPILOGUE
