/*********************************************************************/
/*                                                                   */
/*             Optimized BLAS libraries                              */
/*                     By Kazushige Goto <kgoto@tacc.utexas.edu>     */
/*                                                                   */
/* Copyright (c) The University of Texas, 2005. All rights reserved. */
/* UNIVERSITY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES CONCERNING  */
/* THIS SOFTWARE AND DOCUMENTATION, INCLUDING ANY WARRANTIES OF      */
/* MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE,              */
/* NON-INFRINGEMENT AND WARRANTIES OF PERFORMANCE, AND ANY WARRANTY  */
/* THAT MIGHT OTHERWISE ARISE FROM COURSE OF DEALING OR USAGE OF     */
/* TRADE. NO WARRANTY IS EITHER EXPRESS OR IMPLIED WITH RESPECT TO   */
/* THE USE OF THE SOFTWARE OR DOCUMENTATION.                         */
/* Under no circumstances shall University be liable for incidental, */
/* special, indirect, direct or consequential damages or loss of     */
/* profits, interruption of business, or related expenses which may  */
/* arise from use of Software or Documentation, including but not    */
/* limited to those resulting from defects in Software and/or        */
/* Documentation, or loss or inaccuracy of data of any kind.         */
/*********************************************************************/

#define ASSEMBLER
#include "common.h"

#define P 2000

#ifdef PENTIUM4
#define PREFETCHSIZE 48
#endif

#if defined(OPTERON) || defined(BARCELONA)
#define movsd	movlpd
#define PREFETCHSIZE 64
#endif

#define STACK	16
	
#define OLD_M		 4 + STACK(%esi)
#define OLD_N		 8 + STACK(%esi)
#define OLD_ALPHA	16 + STACK(%esi)
#define OLD_A		24 + STACK(%esi)
#define OLD_LDA		28 + STACK(%esi)
#define OLD_X		32 + STACK(%esi)
#define OLD_INCX	36 + STACK(%esi)
#define OLD_Y		40 + STACK(%esi)
#define OLD_INCY	44 + STACK(%esi)
#define OLD_BUFFER	48 + STACK(%esi)

#define ALPHA	 0(%esp)
#define M	16(%esp)
#define N	20(%esp)
#define A	24(%esp)
#define INCX	36(%esp)
#define Y	40(%esp)
#define INCY	44(%esp)
#define IS	48(%esp)
#define PLDA_M	52(%esp)

#define NLDA	56(%esp)
#define MIN_M	64(%esp)
#define J	68(%esp)
#define LDA	72(%esp)
#define BUFFER  76(%esp)
#define OLD_STACK 80(%esp)

	PROLOGUE

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx

	PROFCODE

	movl	%esp, %esi	# save old stack

	subl	$128, %esp
	andl	$-128, %esp	# align stack

	movl	OLD_BUFFER,   %eax
	movl	%eax, BUFFER
	
	movl	OLD_M, %ebx
	movl	OLD_N, %edx
	movl	OLD_A, %eax
	movl	OLD_LDA, %ecx
	movl	OLD_X, %edi

	movl	%ebx, M
	movl	%edx, N
	movl	%eax, A

	movl	OLD_INCX, %eax
	leal	(,%eax,SIZE), %eax
	movl	%eax, INCX

	movl	OLD_Y, %eax
	movl	%eax, Y

	movl	OLD_INCY, %eax
	leal	(, %eax, SIZE), %eax
	movl	%eax, INCY

	movl	%esi, OLD_STACK

	movsd	OLD_ALPHA,  %xmm3
	unpcklpd %xmm3, %xmm3
	movapd	 %xmm3, ALPHA

	testl	%ebx, %ebx
	jle	.L999
	testl	%edx, %edx
	jle	.L999

	imull	%ecx, %edx
	movl	$P,   %eax
	subl	%edx, %eax

	leal	(, %eax, SIZE), %eax
	leal	(, %ecx, SIZE), %ecx
	movl	%eax, NLDA
	movl	%ecx, LDA
	movl	$0,  %esi
	ALIGN_2

.L10:
	movl	$P, %edx
	movl	M,    %ecx
	subl	%esi, %ecx
	cmpl	%edx, %ecx
	cmovg	%edx, %ecx
	movl	%ecx, MIN_M
	movl	%esi, IS

	movl	BUFFER, %esi

	movl	INCX, %ebx
	cmpl	$SIZE, %ebx
	jne	.L15

	sarl	$3, %ecx
	jle	.L13
	ALIGN_3

.L12:
	movsd	0 * SIZE(%edi), %xmm0
	movhpd	1 * SIZE(%edi), %xmm0
	movsd	2 * SIZE(%edi), %xmm2
	movhpd	3 * SIZE(%edi), %xmm2
	movsd	4 * SIZE(%edi), %xmm4
	movhpd	5 * SIZE(%edi), %xmm4
	movsd	6 * SIZE(%edi), %xmm6
	movhpd	7 * SIZE(%edi), %xmm6
	addl	$8 * SIZE, %edi

	movapd	%xmm0, 0 * SIZE(%esi)
	movapd	%xmm2, 2 * SIZE(%esi)
	movapd	%xmm4, 4 * SIZE(%esi)
	movapd	%xmm6, 6 * SIZE(%esi)

	addl	$8 * SIZE, %esi
	decl	%ecx
	jg	.L12
	ALIGN_3

.L13:
	movl	MIN_M, %ecx
	andl	$7, %ecx
	jle	.L50
	ALIGN_2

.L14:
	movsd	(%edi), %xmm0
	addl	$SIZE, %edi
	movsd	%xmm0, 0 * SIZE(%esi)
	addl	$SIZE, %esi
	decl	%ecx
	jg	.L14
	jmp	.L50
	ALIGN_3

.L15:
	sarl	$3, %ecx
	jle	.L17
	ALIGN_3

.L16:
	movsd	(%edi), %xmm0
	addl	%ebx, %edi
	movhpd	(%edi), %xmm0
	addl	%ebx, %edi

	movsd	(%edi), %xmm2
	addl	%ebx, %edi
	movhpd	(%edi), %xmm2
	addl	%ebx, %edi

	movsd	(%edi), %xmm4
	addl	%ebx, %edi
	movhpd	(%edi), %xmm4
	addl	%ebx, %edi

	movsd	(%edi), %xmm6
	addl	%ebx, %edi
	movhpd	(%edi), %xmm6
	addl	%ebx, %edi

	movapd	%xmm0, 0 * SIZE(%esi)
	movapd	%xmm2, 2 * SIZE(%esi)
	movapd	%xmm4, 4 * SIZE(%esi)
	movapd	%xmm6, 6 * SIZE(%esi)

	addl	$8 * SIZE, %esi
	decl	%ecx
	jg	.L16
	ALIGN_3

.L17:
	movl	MIN_M, %ecx
	andl	$7, %ecx
	jle	.L50
	ALIGN_2

.L18:
	movsd	(%edi), %xmm0
	addl	%ebx, %edi
	movsd	%xmm0, 0 * SIZE(%esi)
	addl	$SIZE, %esi
	decl	%ecx
	jg	.L18
	ALIGN_3

/* Main Routine */

.L50:
	movl	Y, %ebp			# coffset = y

	movl	N, %esi
	sarl	$2, %esi
	movl	%esi, J
	jle	.L100
	ALIGN_3

.L51:
	movl	A, %ebx				# a_offset = a
	movl	LDA, %edx

	leal	(%ebx, %edx), %ecx		# a_offset2 = a + lda
	leal	(%ebx, %edx, 4), %eax

	movl	%eax, A

	movl	BUFFER, %esi

	pxor	%xmm4, %xmm4
	pxor	%xmm5, %xmm5
	pxor	%xmm6, %xmm6
	pxor	%xmm7, %xmm7

	movapd	0 * SIZE(%esi), %xmm0
	movapd	4 * SIZE(%esi), %xmm2

	movl	MIN_M, %eax
	sarl	$3,  %eax
	jle	.L53
	ALIGN_3

.L52:
	movsd	0 * SIZE(%ebx), %xmm1
	movhpd	1 * SIZE(%ebx), %xmm1
	mulpd	%xmm0, %xmm1
#ifdef PENTIUM4
#	prefetchnta	PREFETCHSIZE * SIZE(%ebx)
#endif
	addpd	%xmm1, %xmm4

	movsd	0 * SIZE(%ecx), %xmm1
	movhpd	1 * SIZE(%ecx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm5

	movsd	0 * SIZE(%ebx, %edx, 2), %xmm1
	movhpd	1 * SIZE(%ebx, %edx, 2), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm6

	movsd	0 * SIZE(%ecx, %edx, 2), %xmm1
	movhpd	1 * SIZE(%ecx, %edx, 2), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm7

	movapd	2 * SIZE(%esi), %xmm0

	movsd	2 * SIZE(%ebx), %xmm1
	movhpd	3 * SIZE(%ebx), %xmm1
	mulpd	%xmm0, %xmm1
#ifdef PENTIUM4
#	prefetchnta	PREFETCHSIZE * SIZE(%ecx)
#endif
	addpd	%xmm1, %xmm4

	movsd	2 * SIZE(%ecx), %xmm1
	movhpd	3 * SIZE(%ecx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm5

	movsd	2 * SIZE(%ebx, %edx, 2), %xmm1
	movhpd	3 * SIZE(%ebx, %edx, 2), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm6

	movsd	2 * SIZE(%ecx, %edx, 2), %xmm1
	movhpd	3 * SIZE(%ecx, %edx, 2), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm7

	movapd	 8 * SIZE(%esi), %xmm0

	movsd	4 * SIZE(%ebx), %xmm3
	movhpd	5 * SIZE(%ebx), %xmm3
	mulpd	%xmm2, %xmm3
#ifdef PENTIUM4
#	prefetchnta	PREFETCHSIZE * SIZE(%ebx, %edx, 2)
#endif
	addpd	%xmm3, %xmm4

	movsd	4 * SIZE(%ecx), %xmm3
	movhpd	5 * SIZE(%ecx), %xmm3
	mulpd	%xmm2, %xmm3
	addpd	%xmm3, %xmm5

	movsd	4 * SIZE(%ebx, %edx, 2), %xmm3
	movhpd	5 * SIZE(%ebx, %edx, 2), %xmm3
	mulpd	%xmm2, %xmm3
	addpd	%xmm3, %xmm6

	movsd	4 * SIZE(%ecx, %edx, 2), %xmm3
	movhpd	5 * SIZE(%ecx, %edx, 2), %xmm3
	mulpd	%xmm2, %xmm3
	addpd	%xmm3, %xmm7

	movapd	6 * SIZE(%esi), %xmm2

	movsd	6 * SIZE(%ebx), %xmm3
	movhpd	7 * SIZE(%ebx), %xmm3
	mulpd	%xmm2, %xmm3
#ifdef PENTIUM4
#	prefetchnta	PREFETCHSIZE * SIZE(%ecx, %edx, 2)
#endif
	addpd	%xmm3, %xmm4

	movsd	6 * SIZE(%ecx), %xmm3
	movhpd	7 * SIZE(%ecx), %xmm3
	mulpd	%xmm2, %xmm3
	addpd	%xmm3, %xmm5

	movsd	6 * SIZE(%ebx, %edx, 2), %xmm3
	movhpd	7 * SIZE(%ebx, %edx, 2), %xmm3
	addl	$8 * SIZE, %ebx
	mulpd	%xmm2, %xmm3
	addpd	%xmm3, %xmm6

	movsd	6 * SIZE(%ecx, %edx, 2), %xmm3
	movhpd	7 * SIZE(%ecx, %edx, 2), %xmm3
	addl	$8 * SIZE, %ecx
	mulpd	%xmm2, %xmm3
	addpd	%xmm3, %xmm7

	movapd	12 * SIZE(%esi), %xmm2
	addl	$8 * SIZE, %esi
	decl	%eax
	jg	.L52
	ALIGN_3

.L53:
	movl	MIN_M, %eax
	andl	$7,  %eax
	je	.L55
	ALIGN_3

.L54:
	movsd	0 * SIZE(%ebx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm4

	movsd	0 * SIZE(%ecx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm5

	movsd	0 * SIZE(%ebx, %edx, 2), %xmm1
	addl	$SIZE, %ebx
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm6

	movsd	0 * SIZE(%ecx, %edx, 2), %xmm1
	addl	$SIZE, %ecx
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm7

	movsd	 1 * SIZE(%esi), %xmm0
	addl	$SIZE, %esi

	decl	%eax
	jg	.L54
	ALIGN_3

.L55:
	movapd	%xmm4, %xmm0
	unpckhpd %xmm4, %xmm4
	addsd	 %xmm0, %xmm4
	
	movapd	ALPHA, %xmm3

	movapd	%xmm5, %xmm0
	unpckhpd %xmm5, %xmm5
	addsd	 %xmm0, %xmm5

	movapd	%xmm6, %xmm0
	unpckhpd %xmm6, %xmm6
	addsd	 %xmm0, %xmm6

	movapd	%xmm7, %xmm0
	unpckhpd %xmm7, %xmm7
	addsd	 %xmm0, %xmm7

	mulsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm5
	mulsd	%xmm3, %xmm6
	mulsd	%xmm3, %xmm7

	movl	INCY, %eax
	movl	%ebp, %edx
	cmpl	$SIZE, %eax
	jne	.L56

	movsd	0 * SIZE(%ebp), %xmm1
	movsd	1 * SIZE(%ebp), %xmm2

	addsd	%xmm1, %xmm4
	addsd	%xmm2, %xmm5

	movsd	2 * SIZE(%ebp), %xmm1
	movsd	3 * SIZE(%ebp), %xmm2

	addsd	%xmm1, %xmm6
	addsd	%xmm2, %xmm7

	movsd	%xmm4, 0 * SIZE(%ebp)
	movsd	%xmm5, 1 * SIZE(%ebp)
	movsd	%xmm6, 2 * SIZE(%ebp)
	movsd	%xmm7, 3 * SIZE(%ebp)
	addl	$4 * SIZE, %ebp

	decl	J
	jg	.L51

 	movl	N, %esi
	andl	$3, %esi
	jne	.L100

	jmp	.L99
	ALIGN_3

.L56:
	movsd	0 * SIZE(%edx), %xmm1
	addl	%eax, %edx
	movsd	0 * SIZE(%edx), %xmm2
	addl	%eax, %edx

	addsd	%xmm1, %xmm4
	addsd	%xmm2, %xmm5

	movsd	0 * SIZE(%edx), %xmm1
	addl	%eax, %edx
	movsd	0 * SIZE(%edx), %xmm2
	addl	%eax, %edx

	addsd	%xmm1, %xmm6
	addsd	%xmm2, %xmm7

	movsd	%xmm4, 0 * SIZE(%ebp)
	addl	%eax, %ebp
	movsd	%xmm5, 0 * SIZE(%ebp)
	addl	%eax, %ebp
	movsd	%xmm6, 0 * SIZE(%ebp)
	addl	%eax, %ebp
	movsd	%xmm7, 0 * SIZE(%ebp)
	addl	%eax, %ebp

	decl	J
	jg	.L51

 	movl	N, %esi
	andl	$3, %esi
	jne	.L100

	ALIGN_3

.L99:
	movl	A, %ebx
	addl	NLDA, %ebx
	movl	%ebx, A

	movl	IS, %esi
	addl	$P, %esi
	cmpl	M,  %esi
	jl	.L10
	ALIGN_3

.L999:
	movl	OLD_STACK, %esp
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	ret
	ALIGN_3

.L100:
	movl	N, %esi
	andl	$3, %esi
	cmpl	$3, %esi
	jne	.L110
	ALIGN_3

.L101:
	movl	A, %ebx				# a_offset = a
	movl	LDA, %edx

	leal	(%ebx, %edx), %ecx		# a_offset2 = a + lda
	leal	(%ebx, %edx, 2), %eax
	addl	%edx, %eax

	movl	%eax, A

	movl	BUFFER, %esi

	pxor	%xmm4, %xmm4
	pxor	%xmm5, %xmm5
	pxor	%xmm6, %xmm6

	movapd	0 * SIZE(%esi), %xmm0
	movapd	2 * SIZE(%esi), %xmm2

	movl	MIN_M, %eax
	sarl	$3,  %eax
	jle	.L103
	ALIGN_3

.L102:
	movsd	0 * SIZE(%ebx), %xmm1
	movhpd	1 * SIZE(%ebx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm4

	movsd	0 * SIZE(%ecx), %xmm1
	movhpd	1 * SIZE(%ecx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm5

	movsd	0 * SIZE(%ebx, %edx, 2), %xmm1
	movhpd	1 * SIZE(%ebx, %edx, 2), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm6

	movapd	4 * SIZE(%esi), %xmm0

	movsd	2 * SIZE(%ebx), %xmm1
	movhpd	3 * SIZE(%ebx), %xmm1
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm4

	movsd	2 * SIZE(%ecx), %xmm1
	movhpd	3 * SIZE(%ecx), %xmm1
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm5

	movsd	2 * SIZE(%ebx, %edx, 2), %xmm1
	movhpd	3 * SIZE(%ebx, %edx, 2), %xmm1
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm6

	movapd	6 * SIZE(%esi), %xmm2

	movsd	4 * SIZE(%ebx), %xmm1
	movhpd	5 * SIZE(%ebx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm4

	movsd	4 * SIZE(%ecx), %xmm1
	movhpd	5 * SIZE(%ecx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm5

	movsd	4 * SIZE(%ebx, %edx, 2), %xmm1
	movhpd	5 * SIZE(%ebx, %edx, 2), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm6

	movapd	8 * SIZE(%esi), %xmm0

	movsd	6 * SIZE(%ebx), %xmm1
	movhpd	7 * SIZE(%ebx), %xmm1
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm4

	movsd	6 * SIZE(%ecx), %xmm1
	movhpd	7 * SIZE(%ecx), %xmm1
	addl	$8 * SIZE, %ecx
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm5

	movsd	6 * SIZE(%ebx, %edx, 2), %xmm1
	movhpd	7 * SIZE(%ebx, %edx, 2), %xmm1
	addl	$8 * SIZE, %ebx
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm6

	movapd	10 * SIZE(%esi), %xmm2
	addl	$8 * SIZE, %esi

	decl	%eax
	jg	.L102
	ALIGN_3

.L103:
	movl	MIN_M, %eax
	andl	$7,  %eax
	je	.L105
	ALIGN_3

.L104:
	movsd	0 * SIZE(%ebx), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm4

	movsd	0 * SIZE(%ecx), %xmm1
	addl	$SIZE, %ecx
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm5

	movsd	0 * SIZE(%ebx, %edx, 2), %xmm1
	addl	$SIZE, %ebx
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm6

	movsd	 1 * SIZE(%esi), %xmm0
	addl	$SIZE, %esi

	decl	%eax
	jg	.L104
	ALIGN_3

.L105:
	movapd	%xmm4, %xmm0
	unpckhpd %xmm4, %xmm4
	addsd	 %xmm0, %xmm4
	
	movapd	%xmm5, %xmm0
	unpckhpd %xmm5, %xmm5
	addsd	 %xmm0, %xmm5

	movapd	%xmm6, %xmm0
	unpckhpd %xmm6, %xmm6
	addsd	 %xmm0, %xmm6

	mulsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm5
	mulsd	%xmm3, %xmm6

	movl	INCY, %eax
	movl	%ebp, %edx
	cmpl	$SIZE, %eax
	jne	.L106

	movsd	0 * SIZE(%ebp), %xmm1
	movsd	1 * SIZE(%ebp), %xmm2

	addsd	%xmm1, %xmm4
	addsd	%xmm2, %xmm5

	movsd	2 * SIZE(%ebp), %xmm1

	addsd	%xmm1, %xmm6

	movsd	%xmm4, 0 * SIZE(%ebp)
	movsd	%xmm5, 1 * SIZE(%ebp)
	movsd	%xmm6, 2 * SIZE(%ebp)
	jmp	.L99
	ALIGN_3

.L106:
	movsd	0 * SIZE(%edx), %xmm1
	addl	%eax, %edx
	movsd	0 * SIZE(%edx), %xmm2
	addl	%eax, %edx

	addsd	%xmm1, %xmm4
	addsd	%xmm2, %xmm5

	movsd	0 * SIZE(%edx), %xmm1
	addsd	%xmm1, %xmm6

	movsd	%xmm4, 0 * SIZE(%ebp)
	addl	%eax, %ebp
	movsd	%xmm5, 0 * SIZE(%ebp)
	addl	%eax, %ebp
	movsd	%xmm6, 0 * SIZE(%ebp)
	jmp	.L99
	ALIGN_3

.L110:
	cmpl	$2, %esi
	jne	.L120
	ALIGN_3

.L111:
	movl	A, %ebx				# a_offset = a
	movl	LDA, %edx

	leal	(%ebx, %edx), %ecx		# a_offset2 = a + lda
	leal	(%ebx, %edx, 2), %eax

	movl	%eax, A

	movl	BUFFER, %esi

	pxor	%xmm4, %xmm4
	pxor	%xmm5, %xmm5

	movapd	0 * SIZE(%esi), %xmm0
	movapd	2 * SIZE(%esi), %xmm2

	movl	MIN_M, %eax
	sarl	$3,  %eax
	jle	.L113
	ALIGN_3

.L112:
	movsd	0 * SIZE(%ebx), %xmm1
	movhpd	1 * SIZE(%ebx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm4

	movsd	0 * SIZE(%ecx), %xmm1
	movhpd	1 * SIZE(%ecx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm5

	movapd	4 * SIZE(%esi), %xmm0

	movsd	2 * SIZE(%ebx), %xmm1
	movhpd	3 * SIZE(%ebx), %xmm1
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm4

	movsd	2 * SIZE(%ecx), %xmm1
	movhpd	3 * SIZE(%ecx), %xmm1
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm5

	movapd	6 * SIZE(%esi), %xmm2

	movsd	4 * SIZE(%ebx), %xmm1
	movhpd	5 * SIZE(%ebx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm4

	movsd	4 * SIZE(%ecx), %xmm1
	movhpd	5 * SIZE(%ecx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm5

	movapd	8 * SIZE(%esi), %xmm0

	movsd	6 * SIZE(%ebx), %xmm1
	movhpd	7 * SIZE(%ebx), %xmm1
	addl	$8 * SIZE, %ebx
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm4

	movsd	6 * SIZE(%ecx), %xmm1
	movhpd	7 * SIZE(%ecx), %xmm1
	addl	$8 * SIZE, %ecx
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm5

	movapd	10 * SIZE(%esi), %xmm2
	addl	$8 * SIZE, %esi

	decl	%eax
	jg	.L112
	ALIGN_3

.L113:
	movl	MIN_M, %eax
	andl	$7,  %eax
	je	.L115
	ALIGN_3

.L114:
	movsd	0 * SIZE(%ebx), %xmm1
	addl	$SIZE, %ebx
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm4

	movsd	0 * SIZE(%ecx), %xmm1
	addl	$SIZE, %ecx
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm5

	movsd	 1 * SIZE(%esi), %xmm0
	addl	$SIZE, %esi

	decl	%eax
	jg	.L114
	ALIGN_3

.L115:
	movapd	%xmm4, %xmm0
	unpckhpd %xmm4, %xmm4
	addsd	 %xmm0, %xmm4
	
	movapd	%xmm5, %xmm0
	unpckhpd %xmm5, %xmm5
	addsd	 %xmm0, %xmm5

	mulsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm5

	movl	INCY, %eax
	movl	%ebp, %edx
	cmpl	$SIZE, %eax
	jne	.L116

	movsd	0 * SIZE(%ebp), %xmm1
	movsd	1 * SIZE(%ebp), %xmm2

	addsd	%xmm1, %xmm4
	addsd	%xmm2, %xmm5

	movsd	%xmm4, 0 * SIZE(%ebp)
	movsd	%xmm5, 1 * SIZE(%ebp)
	jmp	.L99
	ALIGN_3

.L116:
	movsd	0 * SIZE(%edx), %xmm1
	addl	%eax, %edx
	movsd	0 * SIZE(%edx), %xmm2

	addsd	%xmm1, %xmm4
	addsd	%xmm2, %xmm5

	movsd	%xmm4, 0 * SIZE(%ebp)
	addl	%eax, %ebp
	movsd	%xmm5, 0 * SIZE(%ebp)
	jmp	.L99
	ALIGN_3

.L120:
	movl	A, %ebx				# a_offset = a
	movl	LDA, %edx

	leal	(%ebx, %edx), %ecx		# a_offset2 = a + lda
	leal	(%ebx, %edx, 1), %eax

	movl	%eax, A

	movl	BUFFER, %esi

	pxor	%xmm4, %xmm4

	movapd	0 * SIZE(%esi), %xmm0
	movapd	2 * SIZE(%esi), %xmm2

	movl	MIN_M, %eax
	sarl	$3,  %eax
	jle	.L123
	ALIGN_3

.L122:
	movsd	0 * SIZE(%ebx), %xmm1
	movhpd	1 * SIZE(%ebx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm4

	movapd	4 * SIZE(%esi), %xmm0

	movsd	2 * SIZE(%ebx), %xmm1
	movhpd	3 * SIZE(%ebx), %xmm1
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm4

	movapd	6 * SIZE(%esi), %xmm2

	movsd	4 * SIZE(%ebx), %xmm1
	movhpd	5 * SIZE(%ebx), %xmm1
	mulpd	%xmm0, %xmm1
	addpd	%xmm1, %xmm4

	movapd	8 * SIZE(%esi), %xmm0

	movsd	6 * SIZE(%ebx), %xmm1
	movhpd	7 * SIZE(%ebx), %xmm1
	addl	$8 * SIZE, %ebx
	mulpd	%xmm2, %xmm1
	addpd	%xmm1, %xmm4

	movapd	10 * SIZE(%esi), %xmm2
	addl	$8 * SIZE, %esi

	decl	%eax
	jg	.L122
	ALIGN_3

.L123:
	movl	MIN_M, %eax
	andl	$7,  %eax
	je	.L125
	ALIGN_3

.L124:
	movsd	0 * SIZE(%ebx), %xmm1
	addl	$SIZE, %ebx
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm4

	movsd	 1 * SIZE(%esi), %xmm0
	addl	$SIZE, %esi

	decl	%eax
	jg	.L124
	ALIGN_3

.L125:
	movapd	%xmm4, %xmm0
	unpckhpd %xmm4, %xmm4
	addsd	 %xmm0, %xmm4
	
	mulsd	%xmm3, %xmm4

	movsd	0 * SIZE(%ebp), %xmm1
	addsd	%xmm1, %xmm4
	movsd	%xmm4, 0 * SIZE(%ebp)
	jmp	.L99

	EPILOGUE
