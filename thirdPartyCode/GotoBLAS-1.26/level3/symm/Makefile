TOPDIR	= ../..
include ../../Makefile.rule

OBJS	=
SOBJS	= ssymmf.$(SUFFIX)
DOBJS	= dsymmf.$(SUFFIX)
QOBJS	= qsymmf.$(SUFFIX)
COBJS	= csymmf.$(SUFFIX)
ZOBJS	= zsymmf.$(SUFFIX)
XOBJS	= xsymmf.$(SUFFIX)

SBLASOBJS = ssymm.$(SUFFIX) \
	ssymm_LU.$(SUFFIX) ssymm_LL.$(SUFFIX) ssymm_RU.$(SUFFIX) ssymm_RL.$(SUFFIX) \
	ssymm_iutcopy.$(SUFFIX) ssymm_iltcopy.$(SUFFIX) \
	ssymm_outcopy.$(SUFFIX) ssymm_oltcopy.$(SUFFIX)

DBLASOBJS = dsymm.$(SUFFIX) \
	dsymm_LU.$(SUFFIX) dsymm_LL.$(SUFFIX) dsymm_RU.$(SUFFIX) dsymm_RL.$(SUFFIX) \
	dsymm_iutcopy.$(SUFFIX) dsymm_iltcopy.$(SUFFIX) \
	dsymm_outcopy.$(SUFFIX) dsymm_oltcopy.$(SUFFIX)

QBLASOBJS = qsymm.$(SUFFIX) \
	qsymm_LU.$(SUFFIX) qsymm_LL.$(SUFFIX) qsymm_RU.$(SUFFIX) qsymm_RL.$(SUFFIX) \
	qsymm_iutcopy.$(SUFFIX) qsymm_iltcopy.$(SUFFIX) \
	qsymm_outcopy.$(SUFFIX) qsymm_oltcopy.$(SUFFIX)

CBLASOBJS = csymm.$(SUFFIX) \
	csymm_LU.$(SUFFIX) csymm_LL.$(SUFFIX) csymm_RU.$(SUFFIX) csymm_RL.$(SUFFIX) \
	csymm_iutcopy.$(SUFFIX) csymm_iltcopy.$(SUFFIX) \
	csymm_outcopy.$(SUFFIX) csymm_oltcopy.$(SUFFIX)

ZBLASOBJS = zsymm.$(SUFFIX) \
	zsymm_LU.$(SUFFIX) zsymm_LL.$(SUFFIX) zsymm_RU.$(SUFFIX) zsymm_RL.$(SUFFIX) \
	zsymm_iutcopy.$(SUFFIX) zsymm_iltcopy.$(SUFFIX) \
	zsymm_outcopy.$(SUFFIX) zsymm_oltcopy.$(SUFFIX)

XBLASOBJS = xsymm.$(SUFFIX) \
	xsymm_LU.$(SUFFIX) xsymm_LL.$(SUFFIX) xsymm_RU.$(SUFFIX) xsymm_RL.$(SUFFIX) \
	xsymm_iutcopy.$(SUFFIX) xsymm_iltcopy.$(SUFFIX) \
	xsymm_outcopy.$(SUFFIX) xsymm_oltcopy.$(SUFFIX)

ifdef SMP
ifndef USE_SIMPLE_THREADED_LEVEL3
SBLASOBJS    += ssymm_thread_LU.$(SUFFIX) ssymm_thread_LL.$(SUFFIX) ssymm_thread_RU.$(SUFFIX) ssymm_thread_RL.$(SUFFIX)
DBLASOBJS    += dsymm_thread_LU.$(SUFFIX) dsymm_thread_LL.$(SUFFIX) dsymm_thread_RU.$(SUFFIX) dsymm_thread_RL.$(SUFFIX)
QBLASOBJS    += qsymm_thread_LU.$(SUFFIX) qsymm_thread_LL.$(SUFFIX) qsymm_thread_RU.$(SUFFIX) qsymm_thread_RL.$(SUFFIX)
CBLASOBJS    += csymm_thread_LU.$(SUFFIX) csymm_thread_LL.$(SUFFIX) csymm_thread_RU.$(SUFFIX) csymm_thread_RL.$(SUFFIX)
ZBLASOBJS    += zsymm_thread_LU.$(SUFFIX) zsymm_thread_LL.$(SUFFIX) zsymm_thread_RU.$(SUFFIX) zsymm_thread_RL.$(SUFFIX)
XBLASOBJS    += xsymm_thread_LU.$(SUFFIX) xsymm_thread_LL.$(SUFFIX) xsymm_thread_RU.$(SUFFIX) xsymm_thread_RL.$(SUFFIX)
endif
endif

ssymm.$(SUFFIX) : symm.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -UCOMPLEX $< -o $(@F)

dsymm.$(SUFFIX) : symm.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -UCOMPLEX $< -o $(@F)

qsymm.$(SUFFIX) : symm.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -UCOMPLEX $< -o $(@F)

csymm.$(SUFFIX) : zsymm.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -DCOMPLEX $< -o $(@F)

zsymm.$(SUFFIX) : zsymm.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -DCOMPLEX $< -o $(@F)

xsymm.$(SUFFIX) : zsymm.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -DCOMPLEX $< -o $(@F)

ssymm_outcopy.$(SUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -UCOMPLEX -DOUTER -ULOWER $< -o $(@F)

ssymm_oltcopy.$(SUFFIX) : symm_copy.c symm_lcopy_16.c  symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -UCOMPLEX -DOUTER -DLOWER $< -o $(@F)

ssymm_iutcopy.$(SUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -UCOMPLEX -UOUTER -ULOWER $< -o $(@F)

ssymm_iltcopy.$(SUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -UCOMPLEX -UOUTER -DLOWER $< -o $(@F)

dsymm_outcopy.$(SUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -UCOMPLEX -DOUTER -ULOWER $< -o $(@F)

dsymm_oltcopy.$(SUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -UCOMPLEX -DOUTER -DLOWER $< -o $(@F)

dsymm_iutcopy.$(SUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -UCOMPLEX -UOUTER -ULOWER $< -o $(@F)

dsymm_iltcopy.$(SUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -UCOMPLEX -UOUTER -DLOWER $< -o $(@F)

qsymm_outcopy.$(SUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -UCOMPLEX -DOUTER -ULOWER $< -o $(@F)

qsymm_oltcopy.$(SUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -UCOMPLEX -DOUTER -DLOWER $< -o $(@F)

qsymm_iutcopy.$(SUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -UCOMPLEX -UOUTER -ULOWER $< -o $(@F)

qsymm_iltcopy.$(SUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -UCOMPLEX -UOUTER -DLOWER $< -o $(@F)

csymm_outcopy.$(SUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -DCOMPLEX -DOUTER -ULOWER $< -o $(@F)

csymm_oltcopy.$(SUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -DCOMPLEX -DOUTER -DLOWER $< -o $(@F)

csymm_iutcopy.$(SUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -DCOMPLEX -UOUTER -ULOWER $< -o $(@F)

csymm_iltcopy.$(SUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -DCOMPLEX -UOUTER -DLOWER $< -o $(@F)

zsymm_outcopy.$(SUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -DCOMPLEX -DOUTER -ULOWER $< -o $(@F)

zsymm_oltcopy.$(SUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -DCOMPLEX -DOUTER -DLOWER $< -o $(@F)

zsymm_iutcopy.$(SUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -DCOMPLEX -UOUTER -ULOWER $< -o $(@F)

zsymm_iltcopy.$(SUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -DCOMPLEX -UOUTER -DLOWER $< -o $(@F)

xsymm_outcopy.$(SUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -DCOMPLEX -DOUTER -ULOWER $< -o $(@F)

xsymm_oltcopy.$(SUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -DCOMPLEX -DOUTER -DLOWER $< -o $(@F)

xsymm_iutcopy.$(SUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -DCOMPLEX -UOUTER -ULOWER $< -o $(@F)

xsymm_iltcopy.$(SUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(CFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -DCOMPLEX -UOUTER -DLOWER $< -o $(@F)

ssymm_LU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

ssymm_LL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
ssymm_RU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

ssymm_RL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

dsymm_LU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

dsymm_LL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
dsymm_RU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

dsymm_RL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

qsymm_LU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

qsymm_LL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
qsymm_RU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

qsymm_RL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

csymm_LU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

csymm_LL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
csymm_RU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

csymm_RL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -UDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

zsymm_LU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

zsymm_LL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
zsymm_RU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

zsymm_RL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

xsymm_LU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

xsymm_LL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
xsymm_RU.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

xsymm_RL.$(SUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(CFLAGS) -DXDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

ssymm_thread_LU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -UDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

ssymm_thread_LL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -UDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
ssymm_thread_RU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -UDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

ssymm_thread_RL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -UDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

dsymm_thread_LU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

dsymm_thread_LL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
dsymm_thread_RU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

dsymm_thread_RL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

qsymm_thread_LU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DXDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

qsymm_thread_LL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DXDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
qsymm_thread_RU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DXDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

qsymm_thread_RL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DXDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

csymm_thread_LU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -UDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

csymm_thread_LL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -UDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
csymm_thread_RU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -UDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

csymm_thread_RL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -UDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

zsymm_thread_LU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

zsymm_thread_LL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
zsymm_thread_RU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

zsymm_thread_RL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

xsymm_thread_LU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DXDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

xsymm_thread_LL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DXDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
xsymm_thread_RU.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DXDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

xsymm_thread_RL.$(SUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(CFLAGS) -DXDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

ssymm.$(PSUFFIX) : symm.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -UCOMPLEX $< -o $(@F)

dsymm.$(PSUFFIX) : symm.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -UCOMPLEX $< -o $(@F)

qsymm.$(PSUFFIX) : symm.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -UCOMPLEX $< -o $(@F)

csymm.$(PSUFFIX) : zsymm.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -DCOMPLEX $< -o $(@F)

zsymm.$(PSUFFIX) : zsymm.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -DCOMPLEX $< -o $(@F)

xsymm.$(PSUFFIX) : zsymm.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -DCOMPLEX $< -o $(@F)

ssymm_outcopy.$(PSUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -UCOMPLEX -DOUTER -ULOWER $< -o $(@F)

ssymm_oltcopy.$(PSUFFIX) : symm_copy.c symm_lcopy_16.c  symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -UCOMPLEX -DOUTER -DLOWER $< -o $(@F)

ssymm_iutcopy.$(PSUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -UCOMPLEX -UOUTER -ULOWER $< -o $(@F)

ssymm_iltcopy.$(PSUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -UCOMPLEX -UOUTER -DLOWER $< -o $(@F)

dsymm_outcopy.$(PSUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -UCOMPLEX -DOUTER -ULOWER $< -o $(@F)

dsymm_oltcopy.$(PSUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -UCOMPLEX -DOUTER -DLOWER $< -o $(@F)

dsymm_iutcopy.$(PSUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -UCOMPLEX -UOUTER -ULOWER $< -o $(@F)

dsymm_iltcopy.$(PSUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -UCOMPLEX -UOUTER -DLOWER $< -o $(@F)

qsymm_outcopy.$(PSUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -UCOMPLEX -DOUTER -ULOWER $< -o $(@F)

qsymm_oltcopy.$(PSUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -UCOMPLEX -DOUTER -DLOWER $< -o $(@F)

qsymm_iutcopy.$(PSUFFIX) : symm_copy.c symm_ucopy_16.c symm_ucopy_8.c symm_ucopy_4.c symm_ucopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -UCOMPLEX -UOUTER -ULOWER $< -o $(@F)

qsymm_iltcopy.$(PSUFFIX) : symm_copy.c symm_lcopy_16.c symm_lcopy_8.c symm_lcopy_4.c symm_lcopy_2.c symm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -UCOMPLEX -UOUTER -DLOWER $< -o $(@F)

csymm_outcopy.$(PSUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -DCOMPLEX -DOUTER -ULOWER $< -o $(@F)

csymm_oltcopy.$(PSUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -DCOMPLEX -DOUTER -DLOWER $< -o $(@F)

csymm_iutcopy.$(PSUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -DCOMPLEX -UOUTER -ULOWER $< -o $(@F)

csymm_iltcopy.$(PSUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -UDOUBLE -DCOMPLEX -UOUTER -DLOWER $< -o $(@F)

zsymm_outcopy.$(PSUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -DCOMPLEX -DOUTER -ULOWER $< -o $(@F)

zsymm_oltcopy.$(PSUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -DCOMPLEX -DOUTER -DLOWER $< -o $(@F)

zsymm_iutcopy.$(PSUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -DCOMPLEX -UOUTER -ULOWER $< -o $(@F)

zsymm_iltcopy.$(PSUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DDOUBLE -DCOMPLEX -UOUTER -DLOWER $< -o $(@F)

xsymm_outcopy.$(PSUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -DCOMPLEX -DOUTER -ULOWER $< -o $(@F)

xsymm_oltcopy.$(PSUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -DCOMPLEX -DOUTER -DLOWER $< -o $(@F)

xsymm_iutcopy.$(PSUFFIX) : zsymm_copy.c zsymm_ucopy_8.c zsymm_ucopy_4.c zsymm_ucopy_2.c zsymm_ucopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -DCOMPLEX -UOUTER -ULOWER $< -o $(@F)

xsymm_iltcopy.$(PSUFFIX) : zsymm_copy.c zsymm_lcopy_8.c zsymm_lcopy_4.c zsymm_lcopy_2.c zsymm_lcopy_1.c
	$(CC) -c $(PFLAGS) $(NO_UNINITIALIZED_WARN) -DXDOUBLE -DCOMPLEX -UOUTER -DLOWER $< -o $(@F)

ssymm_LU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

ssymm_LL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
ssymm_RU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

ssymm_RL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

dsymm_LU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

dsymm_LL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
dsymm_RU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

dsymm_RL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

qsymm_LU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

qsymm_LL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
qsymm_RU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

qsymm_RL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

csymm_LU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

csymm_LL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
csymm_RU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

csymm_RL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -UDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

zsymm_LU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

zsymm_LL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
zsymm_RU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

zsymm_RL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

xsymm_LU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

xsymm_LL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
xsymm_RU.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

xsymm_RL.$(PSUFFIX) : symm_k.c ../gemm/level3.c ../../param.h
	$(CC) -c $(PFLAGS) -DXDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

ssymm_thread_LU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -UDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

ssymm_thread_LL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -UDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
ssymm_thread_RU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -UDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

ssymm_thread_RL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -UDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

dsymm_thread_LU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

dsymm_thread_LL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
dsymm_thread_RU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

dsymm_thread_RL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

qsymm_thread_LU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DXDOUBLE -UCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

qsymm_thread_LL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DXDOUBLE -UCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
qsymm_thread_RU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DXDOUBLE -UCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

qsymm_thread_RL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DXDOUBLE -UCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

csymm_thread_LU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -UDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

csymm_thread_LL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -UDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
csymm_thread_RU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -UDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

csymm_thread_RL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -UDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

zsymm_thread_LU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

zsymm_thread_LL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
zsymm_thread_RU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

zsymm_thread_RL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

xsymm_thread_LU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DXDOUBLE -DCOMPLEX -ULOWER -URSIDE -DNN $< -o $(@F)

xsymm_thread_LL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DXDOUBLE -DCOMPLEX -DLOWER -URSIDE -DNN $< -o $(@F)
				   
xsymm_thread_RU.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DXDOUBLE -DCOMPLEX -ULOWER -DRSIDE -DNN $< -o $(@F)

xsymm_thread_RL.$(PSUFFIX) : symm_k.c ../gemm/level3_thread.c ../../param.h
	$(CC) -c -DTHREADED_LEVEL3 $(PFLAGS) -DXDOUBLE -DCOMPLEX -DLOWER -DRSIDE -DNN $< -o $(@F)

include ../../Makefile.tail
