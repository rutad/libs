/*********************************************************************/
/*                                                                   */
/*             Optimized BLAS libraries                              */
/*                     By Kazushige Goto <kgoto@tacc.utexas.edu>     */
/*                                                                   */
/* Copyright (c) The University of Texas, 2005. All rights reserved. */
/* UNIVERSITY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES CONCERNING  */
/* THIS SOFTWARE AND DOCUMENTATION, INCLUDING ANY WARRANTIES OF      */
/* MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE,              */
/* NON-INFRINGEMENT AND WARRANTIES OF PERFORMANCE, AND ANY WARRANTY  */
/* THAT MIGHT OTHERWISE ARISE FROM COURSE OF DEALING OR USAGE OF     */
/* TRADE. NO WARRANTY IS EITHER EXPRESS OR IMPLIED WITH RESPECT TO   */
/* THE USE OF THE SOFTWARE OR DOCUMENTATION.                         */
/* Under no circumstances shall University be liable for incidental, */
/* special, indirect, direct or consequential damages or loss of     */
/* profits, interruption of business, or related expenses which may  */
/* arise from use of Software or Documentation, including but not    */
/* limited to those resulting from defects in Software and/or        */
/* Documentation, or loss or inaccuracy of data of any kind.         */
/*********************************************************************/

#define ASSEMBLER
#include "common.h"

#define STACK	16
#define ARGS	16
	
#define J	 0 + STACK(%esp)
#define I	 4 + STACK(%esp)
#define KK	 8 + STACK(%esp)
#define KKK	12 + STACK(%esp)

#define M	 4 + STACK + ARGS(%esp)
#define N	 8 + STACK + ARGS(%esp)
#define K	12 + STACK + ARGS(%esp)
#ifdef DOUBLE
#define ALPHA_R	16 + STACK + ARGS(%esp)
#define ALPHA_I	24 + STACK + ARGS(%esp)
#define STACK_A	32 + STACK + ARGS(%esp)
#define STACK_B	36 + STACK + ARGS(%esp)
#define C	40 + STACK + ARGS(%esp)
#define STACK_LDC	44 + STACK + ARGS(%esp)
#define OFFSET	48 + STACK + ARGS(%esp)
#else
#define ALPHA_R	16 + STACK + ARGS(%esp)
#define ALPHA_I	20 + STACK + ARGS(%esp)
#define STACK_A	24 + STACK + ARGS(%esp)
#define STACK_B	28 + STACK + ARGS(%esp)
#define C	32 + STACK + ARGS(%esp)
#define STACK_LDC	36 + STACK + ARGS(%esp)
#define OFFSET	40 + STACK + ARGS(%esp)
#endif

#define A	%edx
#define B	%ecx
#define B_ORIG	%ebx
#define LDC	%ebp

#define ADD1	faddp

#if defined(NN) || defined(CN)
#define ADD2	faddp
#else
#define ADD2	fsubrp
#endif

#if defined(NN) || defined(CC)
#define ADD3	fsubrp
#else
#define ADD3	faddp
#endif

#if defined(NN) || defined(NC)
#define ADD4	faddp
#else
#define ADD4	fsubrp
#endif

#define PREFETCHSIZE (5 + 8 * 10)

/*

  A hint of scheduling is received from following URL

  http://www.netlib.org/atlas/atlas-comm/msg00260.html

  Julian's code is still faster than mine, since Athlon has big
  defect ... So this is a sample coding and please don't expect too
  much.

*/

	PROLOGUE

	subl	$ARGS, %esp	# Generate Stack Frame

	pushl	%ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx

	PROFCODE

#if defined(TRMMKERNEL) && !defined(LEFT)
	movl	OFFSET, %eax
	negl	%eax
	movl	%eax, KK
#endif

	movl	STACK_B, B_ORIG
	movl	STACK_LDC, LDC
	sall	$ZBASE_SHIFT, LDC
	
	subl	$-16 * SIZE, B_ORIG
	subl	$-16 * SIZE, STACK_A

	movl	M, %eax
	testl	%eax, %eax
	jle	.L999

	movl	N, %eax
	testl	%eax, %eax
	jle	.L999

	movl	K, %eax
	testl	%eax, %eax
	jle	.L999
	
	movl	N,   %eax
	sarl	$1,  %eax
	movl	%eax, J
	je	.L20
	ALIGN_3

.L11:
#if defined(TRMMKERNEL) && defined(LEFT)
	movl	OFFSET, %eax
	movl	%eax, KK
#endif	

	movl	STACK_A, A
	movl	C, %edi

#if !defined(TRMMKERNEL) || \
	(defined(TRMMKERNEL) &&  defined(LEFT) &&  defined(TRANSA)) || \
	(defined(TRMMKERNEL) && !defined(LEFT) && !defined(TRANSA))
	movl	B_ORIG, B
#else
	movl	KK,   %eax
	leal	(, %eax, SIZE), %eax
	leal	(B_ORIG, %eax, 4), B
#endif

#ifndef TRMMKERNEL
	movl	K,  %eax
#elif (defined(LEFT) && !defined(TRANSA)) || (!defined(LEFT) && defined(TRANSA))
	movl	K, %eax
	subl	KK, %eax
	movl	%eax, KKK	
#else
	movl	KK, %eax
#ifdef LEFT
	addl	$1, %eax
#else
	addl	$2, %eax
#endif
	movl	%eax, KKK
#endif
	sarl	$3, %eax
	jle	.L13
	ALIGN_4

.L12:	
	movl	 -16 * SIZE(B), %esi
	movl	  -8 * SIZE(B), %esi
	movl	   0 * SIZE(B), %esi
	movl	   8 * SIZE(B), %esi
	movl	  16 * SIZE(B), %esi
	movl	  24 * SIZE(B), %esi
	movl	  32 * SIZE(B), %esi
	movl	  40 * SIZE(B), %esi
	subl	 $-64 * SIZE, B
	decl	  %eax
	jne	  .L12
	ALIGN_3

.L13:
	movl	M, %esi
	movl	%esi, I
	ALIGN_3

.L14:
#if !defined(TRMMKERNEL) || \
	(defined(TRMMKERNEL) &&  defined(LEFT) &&  defined(TRANSA)) || \
	(defined(TRMMKERNEL) && !defined(LEFT) && !defined(TRANSA))
	movl	B_ORIG, B
#else
	movl	KK,   %eax
	leal	(, %eax, SIZE), %eax
	leal	(A,      %eax, 2), A
	leal	(B_ORIG, %eax, 4), B
#endif

	fldz
	fldz
	fldz
	fldz

	FLD	  -8 * SIZE(A)
	FLD	 -16 * SIZE(A)
	FLD	 -16 * SIZE(B)

	movl	$32 * SIZE, %esi

	prefetchw	1 * SIZE(%edi)
	prefetchw	1 * SIZE(%edi, LDC)

#ifndef TRMMKERNEL
	movl	K,  %eax
#elif (defined(LEFT) && !defined(TRANSA)) || (!defined(LEFT) && defined(TRANSA))
	movl	K, %eax
	subl	KK, %eax
	movl	%eax, KKK	
#else
	movl	KK, %eax
#ifdef LEFT
	addl	$1, %eax
#else
	addl	$2, %eax
#endif
	movl	%eax, KKK
#endif
	sarl	$2, %eax
 	je	.L16
	ALIGN_3

.L15:
	fmul	%st(1), %st
	ADD1	%st, %st(3)
	PADDING
	FLD	-15 * SIZE(B)

	fmul	%st(1), %st
	ADD2	%st, %st(4)
	PADDING
	FLD	-14 * SIZE(B)

	fmul	%st(1), %st
	ADD1	%st, %st(5)
	PADDING
	FMUL	-13 * SIZE(B)

	ADD2	%st, %st(5)
	FLD	-15 * SIZE(A)
	FLD	-15 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(3)
	PADDING
	FLD	-16 * SIZE(B)

	fmul	%st(1), %st
	ADD4	%st, %st(4)
	PADDING
	FLD	-13 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(5)
	PADDING
	FMUL	-14 * SIZE(B)

	ADD4	%st, %st(5)
	FLD	-14 * SIZE(A)
	FLD	-12 * SIZE(B)

	fmul	%st(1), %st
	ADD1	%st, %st(3)
	PADDING
	FLD	-11 * SIZE(B)

	fmul	%st(1), %st
	ADD2	%st, %st(4)
	PADDING
	FLD	-10 * SIZE(B)

	fmul	%st(1), %st
	ADD1	%st, %st(5)
	PADDING
	FMUL	 -9 * SIZE(B)

	ADD2	%st, %st(5)
	FLD	-13 * SIZE(A)
	FLD	-11 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(3)
	PADDING
	FLD	-12 * SIZE(B)

	fmul	%st(1), %st
	ADD4	%st, %st(4)
	PADDING
	FLD	 -9 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(5)
	PADDING
	FMUL	-10 * SIZE(B)

	ADD4	%st, %st(5)
	FLD	-12 * SIZE(A)
	FLD	 -8 * SIZE(B)

	fmul	%st(1), %st
	ADD1	%st, %st(3)
	PADDING
	FLD	 -7 * SIZE(B)

	fmul	%st(1), %st
	ADD2	%st, %st(4)
	PADDING
	FLD	 -6 * SIZE(B)

	fmul	%st(1), %st
	ADD1	%st, %st(5)
	PADDING
	FMUL	 -5 * SIZE(B)

	ADD2	%st, %st(5)
	FLD	-11 * SIZE(A)
	FLD	 -7 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(3)
	PADDING
	FLD	 -8 * SIZE(B)

	fmul	%st(1), %st
	ADD4	%st, %st(4)
	PADDING
	FLD	 -5 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(5)
	PADDING
	FMUL	 -6 * SIZE(B)

	ADD4	%st, %st(5)
	FLD	-10 * SIZE(A)
	FLD	 -4 * SIZE(B)

	fmul	%st(1), %st
	ADD1	%st, %st(3)
	PADDING
	FLD	 -3 * SIZE(B)

	fmul	%st(1), %st
	ADD2	%st, %st(4)
	PADDING
	FLD	 -2 * SIZE(B)

	fmul	%st(1), %st
	ADD1	%st, %st(5)
	PADDING
	FMUL	 -1 * SIZE(B)

	ADD2	%st, %st(5)
	FLD	 -9 * SIZE(A)
	FLD	 -3 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(3)
	PADDING
	FLD	 -4 * SIZE(B)

	fmul	%st(1), %st
	ADD4	%st, %st(4)
	PADDING
	FLD	 -1 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(5)
	PADDING
	FMUL	 -2 * SIZE(B)

	ADD4	%st, %st(5)
	FLD	  0 * SIZE(A)

	PADDING prefetch	PREFETCHSIZE * SIZE(A)

	addl	$  8 * SIZE, A
	fxch	%st(1)
	subl	$-16 * SIZE, B

	FLD	-16 * SIZE(B)
	decl	%eax
	jne	.L15
	ALIGN_4

.L16:
#ifndef TRMMKERNEL
	movl	K, %eax
#else
	movl	KKK, %eax
#endif
	and	$3, %eax
	je	.L19
	ALIGN_4


.L17:
	fmul	%st(1), %st
	ADD1	%st, %st(3)
	FLD	-15 * SIZE(B)

	fmul	%st(1), %st
	ADD2	%st, %st(4)
	FLD	-14 * SIZE(B)

	fmul	%st(1), %st
	ADD1	%st, %st(5)
	FMUL	-13 * SIZE(B)

	ADD2	%st, %st(5)
	FLD	-15 * SIZE(A)
	FLD	-15 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(3)
	FLD	-16 * SIZE(B)

	fmul	%st(1), %st
	ADD4	%st, %st(4)
	FLD	-13 * SIZE(B)

	fmul	%st(1), %st
	ADD3	%st, %st(5)
	FMUL	-14 * SIZE(B)

	ADD4	%st, %st(5)
	FLD	-14 * SIZE(A)
	FLD	-12 * SIZE(B)

	addl	$2 * SIZE,A
	addl	$4 * SIZE,B

	decl	%eax
	jne	 .L17
	ALIGN_4

.L19:
	ffreep	%st
	ffreep	%st
	ffreep	%st

	FLD	ALPHA_R
	fmul	%st(1), %st
	FLD	ALPHA_I
	fmul	%st(3), %st
	fsubrp	%st, %st(1)
	fxch	%st(2)
	FMUL	ALPHA_R
	fxch	%st(1)
	FMUL	ALPHA_I
	faddp	%st, %st(1)	

#ifndef TRMMKERNEL
	FADD	1 * SIZE(%edi)
	FST	1 * SIZE(%edi)
	FADD	0 * SIZE(%edi)
	FST	0 * SIZE(%edi)
#else
	FST	1 * SIZE(%edi)
	FST	0 * SIZE(%edi)
#endif

	FLD	ALPHA_R
	fmul	%st(1), %st
	FLD	ALPHA_I
	fmul	%st(3), %st
	fsubrp	%st, %st(1)
	fxch	%st(2)
	FMUL	ALPHA_R
	fxch	%st(1)
	FMUL	ALPHA_I
	faddp	%st, %st(1)	

#ifndef TRMMKERNEL
	FADD	1 * SIZE(%edi,LDC)
	FST	1 * SIZE(%edi,LDC)
	FADD	0 * SIZE(%edi,LDC)
	FST	0 * SIZE(%edi,LDC)
#else
	FST	1 * SIZE(%edi,LDC)
	FST	0 * SIZE(%edi,LDC)
#endif

#if (defined(TRMMKERNEL) &&  defined(LEFT) &&  defined(TRANSA)) || \
    (defined(TRMMKERNEL) && !defined(LEFT) && !defined(TRANSA))
	movl	K, %eax
	subl	KKK, %eax
	leal	(,%eax, SIZE), %eax
	leal	(A, %eax, 2), A
	leal	(B, %eax, 4), B
#endif

#if defined(TRMMKERNEL) && defined(LEFT)
	addl	$1, KK
#endif

	addl	$2 * SIZE, %edi
	decl	I
	jne	.L14

#if defined(TRMMKERNEL) && !defined(LEFT)
	addl	$2, KK
#endif

	leal	(, LDC, 2), %eax
	addl	%eax, C
	movl	B, B_ORIG
	decl	J
	jne	.L11
	ALIGN_4

.L20:
	movl	N,   %eax
	andl	$1,  %eax
	je	.L999
	ALIGN_3

#if defined(TRMMKERNEL) && defined(LEFT)
	movl	OFFSET, %eax
	movl	%eax, KK
#endif	

	movl	STACK_A, A
	movl	C, %edi

#if !defined(TRMMKERNEL) || \
	(defined(TRMMKERNEL) &&  defined(LEFT) &&  defined(TRANSA)) || \
	(defined(TRMMKERNEL) && !defined(LEFT) && !defined(TRANSA))
	movl	B_ORIG, B
#else
	movl	KK,   %eax
	leal	(, %eax, SIZE), %eax
	leal	(B_ORIG, %eax, 2), B
#endif

#ifndef TRMMKERNEL
	movl	K,  %eax
#elif (defined(LEFT) && !defined(TRANSA)) || (!defined(LEFT) && defined(TRANSA))
	movl	K, %eax
	subl	KK, %eax
	movl	%eax, KKK	
#else
	movl	KK, %eax
#ifdef LEFT
	addl	$1, %eax
#else
	addl	$1, %eax
#endif
	movl	%eax, KKK
#endif
	sarl	$4, %eax
	jle	.L23
	ALIGN_4

.L22:	
	movl	 -16 * SIZE(B), %esi
	movl	  -8 * SIZE(B), %esi
	movl	   0 * SIZE(B), %esi
	movl	   8 * SIZE(B), %esi
	movl	  16 * SIZE(B), %esi
	movl	  24 * SIZE(B), %esi
	movl	  32 * SIZE(B), %esi
	movl	  40 * SIZE(B), %esi
	subl	 $-64 * SIZE, B
	decl	  %eax
	jne	  .L22
	ALIGN_3

.L23:
	movl	M, %esi
	movl	%esi, I
	ALIGN_3

.L24:
#if !defined(TRMMKERNEL) || \
	(defined(TRMMKERNEL) &&  defined(LEFT) &&  defined(TRANSA)) || \
	(defined(TRMMKERNEL) && !defined(LEFT) && !defined(TRANSA))
	movl	B_ORIG, B
#else
	movl	KK,   %eax
	leal	(, %eax, SIZE), %eax
	leal	(A,      %eax, 2), A
	leal	(B_ORIG, %eax, 2), B
#endif

	fldz
	fldz
	fldz
	fldz

	FLD	-16 * SIZE(A)
	FLD	-16 * SIZE(B)

	prefetchw	1 * SIZE(%edi)

#ifndef TRMMKERNEL
	movl	K,  %eax
#elif (defined(LEFT) && !defined(TRANSA)) || (!defined(LEFT) && defined(TRANSA))
	movl	K, %eax
	subl	KK, %eax
	movl	%eax, KKK	
#else
	movl	KK, %eax
#ifdef LEFT
	addl	$1, %eax
#else
	addl	$1, %eax
#endif
	movl	%eax, KKK
#endif
	sarl	$2, %eax
 	je	.L26
	ALIGN_3

.L25:
	fmul	%st(1), %st
	PADDING
	ADD1	%st, %st(2)
	FMUL	-15 * SIZE(B)

	ADD2	%st, %st(2)
	FLD	-15 * SIZE(A)
	FLD	-16 * SIZE(B)

	fmul	%st(1), %st
	PADDING
	ADD4	%st, %st(4)
	FMUL	-15 * SIZE(B)

	ADD3	%st, %st(4)
	FLD	-14 * SIZE(A)
	FLD	-14 * SIZE(B)

	fmul	%st(1), %st
	PADDING
	ADD1	%st, %st(2)
	FMUL	-13 * SIZE(B)

	ADD2	%st, %st(2)
	FLD	-13 * SIZE(A)
	FLD	-14 * SIZE(B)

	fmul	%st(1), %st
	PADDING
	ADD4	%st, %st(4)
	FMUL	-13 * SIZE(B)

	ADD3	%st, %st(4)
	FLD	-12 * SIZE(A)
	FLD	-12 * SIZE(B)

	fmul	%st(1), %st
	PADDING
	ADD1	%st, %st(2)
	FMUL	-11 * SIZE(B)

	ADD2	%st, %st(2)
	FLD	-11 * SIZE(A)
	FLD	-12 * SIZE(B)

	fmul	%st(1), %st
	PADDING
	ADD4	%st, %st(4)
	FMUL	-11 * SIZE(B)

	ADD3	%st, %st(4)
	FLD	-10 * SIZE(A)
	FLD	-10 * SIZE(B)

	fmul	%st(1), %st
	PADDING
	ADD1	%st, %st(2)
	FMUL	 -9 * SIZE(B)

	ADD2	%st, %st(2)
	FLD	 -9 * SIZE(A)
	FLD	-10 * SIZE(B)

	fmul	%st(1), %st
	PADDING
	ADD4	%st, %st(4)
	FMUL	 -9 * SIZE(B)

	ADD3	%st, %st(4)
	FLD	 -8 * SIZE(A)
	FLD	 -8 * SIZE(B)

	addl	$8 * SIZE,A
	addl	$8 * SIZE,B

	decl	%eax
	jne	.L25
	ALIGN_4

.L26:
#ifndef TRMMKERNEL
	movl	K, %eax
#else
	movl	KKK, %eax
#endif
	and	$3, %eax
	je	.L29
	ALIGN_4

.L27:
	fmul	%st(1), %st
	PADDING
	ADD1	%st, %st(2)
	FMUL	-15 * SIZE(B)

	ADD2	%st, %st(2)
	FLD	-15 * SIZE(A)
	FLD	-16 * SIZE(B)

	fmul	%st(1), %st
	PADDING
	ADD4	%st, %st(4)
	FMUL	-15 * SIZE(B)

	ADD3	%st, %st(4)
	FLD	-14 * SIZE(A)
	FLD	-14 * SIZE(B)

	addl	$2 * SIZE,A
	addl	$2 * SIZE,B

	decl	%eax
	jne	 .L27
	ALIGN_4

.L29:
	ffreep	%st
	ffreep	%st

	faddp	%st, %st(3)
	faddp	%st, %st(1)

	fxch	%st(1)

	FLD	ALPHA_R
	fmul	%st(1), %st
	FLD	ALPHA_I
	fmul	%st(3), %st
	fsubrp	%st, %st(1)
	fxch	%st(2)
	FMUL	ALPHA_R
	fxch	%st(1)
	FMUL	ALPHA_I
	faddp	%st, %st(1)	

#ifndef TRMMKERNEL
	FADD	1 * SIZE(%edi)
	FST	1 * SIZE(%edi)
	FADD	0 * SIZE(%edi)
	FST	0 * SIZE(%edi)
#else
	FST	1 * SIZE(%edi)
	FST	0 * SIZE(%edi)
#endif

#if (defined(TRMMKERNEL) &&  defined(LEFT) &&  defined(TRANSA)) || \
    (defined(TRMMKERNEL) && !defined(LEFT) && !defined(TRANSA))
	movl	K, %eax
	subl	KKK, %eax
	leal	(,%eax, SIZE), %eax
	leal	(A, %eax, 2), A
	leal	(B, %eax, 2), B
#endif

#if defined(TRMMKERNEL) && defined(LEFT)
	addl	$1, KK
#endif

	addl	$2 * SIZE, %edi
	decl	I
	jne	.L24

#if defined(TRMMKERNEL) && !defined(LEFT)
	addl	$1, KK
#endif

	addl	LDC, C
	movl	B, B_ORIG
	ALIGN_4

.L999:
	popl	%ebx
	popl	%esi
	popl	%edi
	popl	%ebp
	addl	$ARGS, %esp
	ret

	EPILOGUE
