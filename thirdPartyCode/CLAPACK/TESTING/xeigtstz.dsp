# Microsoft Developer Studio Project File - Name="xeigtstz" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=xeigtstz - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xeigtstz.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xeigtstz.mak" CFG="xeigtstz - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xeigtstz - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "xeigtstz - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xeigtstz - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Eig_Release"
# PROP Intermediate_Dir "Eig_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Release\tmglib.lib ..\Release\clapack.lib ..\blas\Release\blas.lib ..\f2clibs\Release\libF77.lib ..\f2clibs\Release\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "xeigtstz - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "xeigtstz___Win32_Debug"
# PROP BASE Intermediate_Dir "xeigtstz___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Eig_Debug"
# PROP Intermediate_Dir "Eig_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\tmglib.lib ..\Debug\clapack.lib ..\blas\Debug\blas.lib ..\f2clibs\Debug\libF77.lib ..\f2clibs\Debug\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "xeigtstz - Win32 Release"
# Name "xeigtstz - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "aeigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\alahdg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alareq.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alasum.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alasvm.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chkxer.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ilaenv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\xerbla.c
# End Source File
# Begin Source File

SOURCE=.\Eig\xlaenv.c
# End Source File
# Begin Source File

SOURCE=.\EIG\zget54.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\zlakf2.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\zlatm5.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\zlatm6.c
# End Source File
# End Group
# Begin Group "dzigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\dlafts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlahd2.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlasum.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlatb9.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dstech.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dstect.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsvdch.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsvdct.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsxt1.c
# End Source File
# End Group
# Begin Group "zeigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\zbdt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zbdt02.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zbdt03.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkbb.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkbk.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkbl.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkec.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkee.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkgk.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkgl.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkhb.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zchkst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zckglm.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zckgqr.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zckgsv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zcklse.c
# End Source File
# Begin Source File

SOURCE=.\EIG\zdrges.c
# End Source File
# Begin Source File

SOURCE=.\EIG\zdrgev.c
# End Source File
# Begin Source File

SOURCE=.\EIG\zdrgsx.c
# End Source File
# Begin Source File

SOURCE=.\EIG\zdrgvx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zdrvbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zdrves.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zdrvev.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zdrvgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zdrvsg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zdrvst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zdrvsx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zdrvvx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zerrbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zerrec.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zerred.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zerrgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zerrhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zerrst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget02.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget10.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget23.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget24.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget35.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget36.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget37.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget38.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget51.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zget52.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zglmts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zgqrts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zgrqts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zgsvts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zhbt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zhet21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zhet22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zhpt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zhst01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zlarfy.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zlarhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zlatm4.c
# End Source File
# Begin Source File

SOURCE=.\EIG\zlctes.c
# End Source File
# Begin Source File

SOURCE=.\EIG\zlctsx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zlsets.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zsbmv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zsgt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zslect.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zstt21.c
# End Source File
# Begin Source File

SOURCE=.\EIG\zstt22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zunt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\zunt03.c
# End Source File
# End Group
# End Group
# End Target
# End Project
