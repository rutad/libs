# Microsoft Developer Studio Project File - Name="xeigtstd" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=xeigtstd - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xeigtstd.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xeigtstd.mak" CFG="xeigtstd - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xeigtstd - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "xeigtstd - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xeigtstd - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Eig_Release"
# PROP Intermediate_Dir "Eig_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Release\tmglib.lib ..\Release\clapack.lib ..\blas\Release\blas.lib ..\f2clibs\Release\libF77.lib ..\f2clibs\Release\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "xeigtstd - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "xeigtstd___Win32_Debug"
# PROP BASE Intermediate_Dir "xeigtstd___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Eig_Debug"
# PROP Intermediate_Dir "Eig_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\tmglib.lib ..\Debug\clapack.lib ..\blas\Debug\blas.lib ..\f2clibs\Debug\libF77.lib ..\f2clibs\Debug\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "xeigtstd - Win32 Release"
# Name "xeigtstd - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "aeigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\alahdg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alareq.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alasum.c
# End Source File
# Begin Source File

SOURCE=.\Eig\alasvm.c
# End Source File
# Begin Source File

SOURCE=.\Eig\chkxer.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ilaenv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\xerbla.c
# End Source File
# Begin Source File

SOURCE=.\Eig\xlaenv.c
# End Source File
# End Group
# Begin Group "deigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\dbdt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dbdt02.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dbdt03.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkbb.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkbk.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkbl.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkec.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkee.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkgk.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkgl.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchksb.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dchkst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dckglm.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dckgqr.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dckgsv.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dcklse.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ddrvbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ddrves.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ddrvev.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ddrvgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ddrvsg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ddrvst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ddrvsx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\ddrvvx.c
# End Source File
# Begin Source File

SOURCE=.\Eig\derrbd.c
# End Source File
# Begin Source File

SOURCE=.\Eig\derrec.c
# End Source File
# Begin Source File

SOURCE=.\Eig\derred.c
# End Source File
# Begin Source File

SOURCE=.\Eig\derrgg.c
# End Source File
# Begin Source File

SOURCE=.\Eig\derrhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\derrst.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget02.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget10.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget23.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget24.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget31.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget32.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget33.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget34.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget35.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget36.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget37.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget38.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget39.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget51.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget52.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dget53.c
# End Source File
# Begin Source File

SOURCE=.\EIG\dget54.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dglmts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dgqrts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dgrqts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dgsvts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dhst01.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\dlakf2.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlarfy.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlarhs.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlatm4.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\dlatm5.c
# End Source File
# Begin Source File

SOURCE=.\MATGEN\dlatm6.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlsets.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dort01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dort03.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsbt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsgt01.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dslect.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dspt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dstt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dstt22.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsyt21.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsyt22.c
# End Source File
# End Group
# Begin Group "dzigtst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Eig\dlafts.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlahd2.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlasum.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dlatb9.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dstech.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dstect.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsvdch.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsvdct.c
# End Source File
# Begin Source File

SOURCE=.\Eig\dsxt1.c
# End Source File
# End Group
# Begin Source File

SOURCE=.\EIG\ddrges.c
# End Source File
# Begin Source File

SOURCE=.\EIG\ddrgev.c
# End Source File
# Begin Source File

SOURCE=.\EIG\ddrgsx.c
# End Source File
# Begin Source File

SOURCE=.\EIG\ddrgvx.c
# End Source File
# Begin Source File

SOURCE=.\EIG\dlctes.c
# End Source File
# Begin Source File

SOURCE=.\EIG\dlctsx.c
# End Source File
# End Group
# End Target
# End Project
