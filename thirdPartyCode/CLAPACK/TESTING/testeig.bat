@echo off
set MODE=Eig_Release
:
:
: ======== SINGLE EIG TESTS ===========================
:
	@echo NEP: Data file for testing Nonsymmetric Eigenvalue Problem routines
	%MODE\xeigtsts < nep.in >& s-nep.out

	@echo SEP: Data file for testing Symmetric Eigenvalue Problem routines
	%MODE\xeigtsts < sep.in >& s-sep.out

	@echo SVD: Data file for testing Singular Value Decomposition routines
	%MODE\xeigtsts < svd.in >& s-svd.out

	@echo SEC: Data file for testing REAL Eigen Condition Routines
	%MODE\xeigtsts < sec.in >& s-sec.out

	@echo SEV: Data file for testing REAL Nonsymmetric Eigenvalue Driver
	%MODE\xeigtsts < sed.in >& s-sed.out

	@echo SGG: Data file for testing REAL Nonsymmetric Generalized Eigenvalue Problem routines
	%MODE\xeigtsts < sgg.in >& s-sgg.out

	@echo SSB: Data file for testing REAL Symmetric Eigenvalue Problem routines
	%MODE\xeigtsts < ssb.in >& s-ssb.out

	@echo SSG: Data file for testing REAL Symmetric Generalized Eigenvalue Problem routines
	%MODE\xeigtsts < ssg.in >& s-ssg.out

	@echo SGEBAL: Data file for testing the balancing of a REAL general matrix
	%MODE\xeigtsts < sbal.in >& s-sbal.out

	@echo SGEBAK: Data file for testing the back transformation of a REAL balanced matrix
	%MODE\xeigtsts < sbak.in >& s-sbak.out

	@echo SGGBAL: Data file for testing the balancing of a pair of REAL general matrices
	%MODE\xeigtsts < sgbal.in >& s-sgbal.out

	@echo SGGBAK: Data file for testing the back transformation of a pair of REAL balanced matrices
	%MODE\xeigtsts < sgbak.in >& s-sgbak.out

	@echo SBB:  Data file for testing banded Singular Value Decomposition routines
	%MODE\xeigtsts < sbb.in >& s-sbb.out

	@echo GLM: Data file for testing Generalized Linear Regression Model routines
	%MODE\xeigtsts < glm.in >& s-glm.out

	@echo GQR: Data file for testing Generalized QR and RQ factorization routines
	%MODE\xeigtsts < gqr.in >& s-gqr.out

	@echo GSV: Data file for testing Generalized Singular Value Decomposition routines
	%MODE\xeigtsts < gsv.in >& s-gsv.out

	@echo LSE: Data file for testing Constrained Linear Least Squares routines
	%MODE\xeigtsts < lse.in >& s-lse.out
:
: ======== COMPLEX EIG TESTS ===========================

	@echo NEP: Data file for testing Nonsymmetric Eigenvalue Problem routines
	%MODE\xeigtstc < nep.in >& c-nep.out

	@echo SEP: Data file for testing Symmetric Eigenvalue Problem routines
	%MODE\xeigtstc < sep.in >& c-sep.out

	@echo SVD: Data file for testing Singular Value Decomposition routines
	%MODE\xeigtstc < svd.in >& c-svd.out

	@echo CEC: Data file for testing COMPLEX Eigen Condition Routines
	%MODE\xeigtstc < cec.in >& c-cec.out

	@echo CES: Data for the testing COMPLEX Nonsymmetric Schur Form Driver
	%MODE\xeigtstc < ced.in >& c-ced.out

	@echo CGG: Data file for testing COMPLEX Nonsymmetric Generalized Eigenvalue Problem routines
	%MODE\xeigtstc < cgg.in >& c-cgg.out

	@echo CHB: Data file for testing Hermitian Eigenvalue Problem routines
	%MODE\xeigtstc < csb.in >& c-csb.out

	@echo CSG: Data file for testing Symmetric Generalized Eigenvalue Problem routines
	%MODE\xeigtstc < csg.in >& c-csg.out

	@echo CGEBAL: Data file for testing the balancing of a COMPLEX general matrix
	%MODE\xeigtstc < cbal.in >& c-cbal.out

	@echo CGEBAK: Data file for testing the back transformation of a COMPLEX balanced matrix
	%MODE\xeigtstc < cbak.in >& c-cbak.out

	@echo CGGBAL: Data file for testing the balancing of a pair of COMPLEX general matrices
	%MODE\xeigtstc < cgbal.in >& c-cgbal.out

	@echo CGGBAK: Data file for testing the back transformation of a pair of COMPLEX balanced matrices
	%MODE\xeigtstc < cgbak.in >& c-cgbak.out

	@echo CBB:  Data file for testing banded Singular Value Decomposition routines
	%MODE\xeigtstc < cbb.in >& c-cbb.out

	@echo GLM: Data file for testing Generalized Linear Regression Model routines
	%MODE\xeigtstc < glm.in >& c-glm.out

	@echo GQR: Data file for testing Generalized QR and RQ factorization routines
	%MODE\xeigtstc < gqr.in >& c-gqr.out

	@echo GSV: Data file for testing Generalized Singular Value Decomposition routines
	%MODE\xeigtstc < gsv.in >& c-gsv.out

	@echo LSE: Data file for testing Constrained Linear Least Squares routines
	%MODE\xeigtstc < lse.in >& c-lse.out
:
: ======== DOUBLE EIG TESTS ===========================

	@echo NEP: Data file for testing Nonsymmetric Eigenvalue Problem routines
	%MODE\xeigtstd < nep.in >& d-nep.out

	@echo SEP: Data file for testing Symmetric Eigenvalue Problem routines
	%MODE\xeigtstd < sep.in >& d-sep.out

	@echo SVD: Data file for testing Singular Value Decomposition routines
	%MODE\xeigtstd < svd.in >& d-svd.out

	@echo DEC: Data file for testing DOUBLE PRECISION Eigen Condition Routines
	%MODE\xeigtstd < dec.in >& d-dec.out

	@echo DEV: Data file for DOUBLE PRECISION Nonsymmetric Eigenvalue Driver
	%MODE\xeigtstd < ded.in >& d-ded.out

	@echo DGG: Data file for testing DOUBLE PRECISION Nonsymmetric Generalized Eigenvalue Problem routines
	%MODE\xeigtstd < dgg.in >& d-dgg.out

	@echo DSB: Data file for testing DOUBLE PRECISION Symmetric Eigenvalue Problem routines
	%MODE\xeigtstd < dsb.in >& d-dsb.out

	@echo DSG: Data file for testing DOUBLE PRECISION Symmetric Generalized Eigenvalue Problem routines
	%MODE\xeigtstd < dsg.in >& d-dsg.out

	@echo DGEBAL: Data file for testing the balancing of a DOUBLE PRECISION general matrix
	%MODE\xeigtstd < dbal.in >& d-dbal.out

	@echo DGEBAK:  Data file for testing the back transformation of a DOUBLE PRECISION balanced matrix
	%MODE\xeigtstd < dbak.in >& d-dbak.out

	@echo DGGBAL: Data file for testing the balancing of a pair of DOUBLE PRECISION general matrices
	%MODE\xeigtstd < dgbal.in >& d-dgbal.out

	@echo DGGBAK: Data file for testing the back transformation of a pair of DOUBLE PRECISION balanced matrices
	%MODE\xeigtstd < dgbak.in >& d-dgbak.out

	@echo DBB:  Data file for testing banded Singular Value Decomposition routines
	%MODE\xeigtstd < dbb.in >& d-dbb.out

	@echo GLM: Data file for testing Generalized Linear Regression Model routines
	%MODE\xeigtstd < glm.in >& d-glm.out

	@echo GQR: Data file for testing Generalized QR and RQ factorization routines
	%MODE\xeigtstd < gqr.in >& d-gqr.out

	@echo GSV: Data file for testing Generalized Singular Value Decomposition routines
	%MODE\xeigtstd < gsv.in >& d-gsv.out

	@echo LSE: Data file for testing Constrained Linear Least Squares routines
	%MODE\xeigtstd < lse.in >& d-lse.out
:
: ======== COMPLEX16 EIG TESTS ===========================

	@echo NEP: Data file for testing Nonsymmetric Eigenvalue Problem routines
	%MODE\xeigtstz < nep.in >& z-nep.out

	@echo SEP: Data file for testing Symmetric Eigenvalue Problem routines
	%MODE\xeigtstz < sep.in >& z-sep.out

	@echo SVD: Data file for testing Singular Value Decomposition routines
	%MODE\xeigtstz < svd.in >& z-svd.out

	@echo ZEC: Data file for testing COMPLEX16 Eigen Condition Routines
	%MODE\xeigtstz < zec.in >& z-zec.out

	@echo ZES: Data file for testing COMPLEX16 Nonsymmetric Schur Form Driver
	%MODE\xeigtstz < zed.in >& z-zed.out

	@echo ZGG: Data file for testing COMPLEX16 Nonsymmetric Generalized Eigenvalue Problem routines
	%MODE\xeigtstz < zgg.in >& z-zgg.out

	@echo ZHB: Data file for testing Hermitian Eigenvalue Problem routines
	%MODE\xeigtstz < zsb.in >& z-zsb.out

	@echo ZSG: Data file for testing Symmetric Generalized Eigenvalue Problem routines
	%MODE\xeigtstz < zsg.in >& z-zsg.out

	@echo ZGEBAL: Data file for testing the balancing of a COMPLEX16 general matrix
	%MODE\xeigtstz < zbal.in >& z-zbal.out

	@echo ZGEBAK: Data file for testing the back transformation of a COMPLEX16 balanced matrix
	%MODE\xeigtstz < zbak.in >& z-zbak.out

	@echo ZGGBAL: Data file for testing the balancing of a pair of COMPLEX general matrices
	%MODE\xeigtstz < zgbal.in >& z-zgbal.out

	@echo ZGGBAK: Data file for testing the back transformation of a pair of COMPLEX16 balanced matrices
	%MODE\xeigtstz < zgbak.in >& z-zgbak.out

	@echo ZBB:  Data file for testing banded Singular Value Decomposition routines
	%MODE\xeigtstz < zbb.in >& z-zbb.out

	@echo GLM: Data file for testing Generalized Linear Regression Model routines
	%MODE\xeigtstz < glm.in >& z-glm.out

	@echo GQR: Data file for testing Generalized QR and RQ factorization routines
	%MODE\xeigtstz < gqr.in >& z-gqr.out

	@echo GSV: Data file for testing Generalized Singular Value Decomposition routines
	%MODE\xeigtstz < gsv.in >& z-gsv.out

	@echo LSE: Data file for testing Constrained Linear Least Squares routines
	%MODE\xeigtstz < lse.in >& z-lse.out
: ==============================================================================


