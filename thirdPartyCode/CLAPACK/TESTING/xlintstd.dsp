# Microsoft Developer Studio Project File - Name="xlintstd" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=xlintstd - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xlintstd.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xlintstd.mak" CFG="xlintstd - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xlintstd - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "xlintstd - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xlintstd - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Release\tmglib.lib ..\Release\clapack.lib ..\blas\Release\blas.lib ..\f2clibs\Release\libF77.lib ..\f2clibs\Release\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "xlintstd - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\tmglib.lib ..\Debug\clapack.lib ..\blas\Debug\blas.lib ..\f2clibs\Debug\libF77.lib ..\f2clibs\Debug\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "xlintstd - Win32 Release"
# Name "xlintstd - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "alintst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Lin\aladhd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alaerh.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alaesm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alahd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alareq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alasum.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alasvm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\chkxer.c
# End Source File
# Begin Source File

SOURCE=.\LIN\icopy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ilaenv.c
# End Source File
# Begin Source File

SOURCE=.\Lin\xerbla.c
# End Source File
# Begin Source File

SOURCE=.\Lin\xlaenv.c
# End Source File
# End Group
# Begin Group "dlintst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Lin\dchkaa.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkeq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkgb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchklq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkpb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkpp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkpt.c
# End Source File
# Begin Source File

SOURCE=.\LIN\dchkq3.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkql.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkqp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkqr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchkrq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchksp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchksy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchktb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchktp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchktr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dchktz.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvgb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvpb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvpp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvpt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ddrvsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrlq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrql.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrqp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrqr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrrq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrtr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrtz.c
# End Source File
# Begin Source File

SOURCE=.\Lin\derrvx.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgbt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgelqs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgeqls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgeqrs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgerqs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dget01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dget02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dget03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dget04.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dget06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dget07.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgtt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgtt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dgtt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlaord.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlaptm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlarhs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlatb4.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlattb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlattp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlattr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlavsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlavsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlqt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlqt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dlqt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dpbt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dpbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dpbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dpot01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dpot02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dpot03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dpot05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dppt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dppt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dppt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dppt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dptt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dptt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dptt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqlt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqlt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqlt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt11.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt12.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt13.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt14.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt15.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt16.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dqrt17.c
# End Source File
# Begin Source File

SOURCE=.\Lin\drqt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\drqt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\drqt03.c
# End Source File
# Begin Source File

SOURCE=.\LIN\drzt01.c
# End Source File
# Begin Source File

SOURCE=.\LIN\drzt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dspt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dsyt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtbt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtbt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtpt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtpt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtpt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtpt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtrt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtrt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtrt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtrt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtrt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtzt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\dtzt02.c
# End Source File
# End Group
# End Group
# End Target
# End Project
