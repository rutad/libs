# Microsoft Developer Studio Project File - Name="xlintsts" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=xlintsts - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xlintsts.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xlintsts.mak" CFG="xlintsts - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xlintsts - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "xlintsts - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xlintsts - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Release\tmglib.lib ..\Release\clapack.lib ..\blas\Release\blas.lib ..\f2clibs\Release\libF77.lib ..\f2clibs\Release\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "xlintsts - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\tmglib.lib ..\Debug\clapack.lib ..\blas\Debug\blas.lib ..\f2clibs\Debug\libF77.lib ..\f2clibs\Debug\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "xlintsts - Win32 Release"
# Name "xlintsts - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "alintst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Lin\aladhd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alaerh.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alaesm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alahd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alareq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alasum.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alasvm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\chkxer.c
# End Source File
# Begin Source File

SOURCE=.\LIN\icopy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ilaenv.c
# End Source File
# Begin Source File

SOURCE=.\Lin\xerbla.c
# End Source File
# Begin Source File

SOURCE=.\Lin\xlaenv.c
# End Source File
# End Group
# Begin Group "slintst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Lin\schkaa.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkeq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkgb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schklq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkpb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkpp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkpt.c
# End Source File
# Begin Source File

SOURCE=.\LIN\schkq3.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkql.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkqp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkqr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schkrq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schksp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schksy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schktb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schktp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schktr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\schktz.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvgb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvpb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvpp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvpt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sdrvsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrlq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrql.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrqp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrqr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrrq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrtr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrtz.c
# End Source File
# Begin Source File

SOURCE=.\Lin\serrvx.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgbt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgelqs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgeqls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgeqrs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgerqs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sget01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sget02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sget03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sget04.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sget06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sget07.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgtt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgtt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sgtt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slaptm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slarhs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slatb4.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slattb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slattp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slattr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slavsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slavsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slqt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slqt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slqt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\spbt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\spbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\spbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\spot01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\spot02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\spot03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\spot05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sppt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sppt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sppt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sppt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sptt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sptt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sptt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqlt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqlt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqlt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt11.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt12.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt13.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt14.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt15.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt16.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sqrt17.c
# End Source File
# Begin Source File

SOURCE=.\Lin\srqt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\srqt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\srqt03.c
# End Source File
# Begin Source File

SOURCE=.\LIN\srzt01.c
# End Source File
# Begin Source File

SOURCE=.\LIN\srzt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sspt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ssyt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stbt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stbt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stpt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stpt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stpt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stpt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\strt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\strt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\strt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\strt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\strt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stzt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\stzt02.c
# End Source File
# End Group
# Begin Source File

SOURCE=.\Lin\slaord.c
# End Source File
# End Group
# End Target
# End Project
