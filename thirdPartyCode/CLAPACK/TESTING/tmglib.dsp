# Microsoft Developer Studio Project File - Name="tmglib" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=tmglib - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "tmglib.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "tmglib.mak" CFG="tmglib - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "tmglib - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "tmglib - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "tmglib - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "o:\clapack\f2clibs" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "tmglib - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "tmglib___Win32_Debug"
# PROP BASE Intermediate_Dir "tmglib___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "D:\CLAPACK\F2Clibs" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "tmglib - Win32 Release"
# Name "tmglib - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Matgen\clagge.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\claghe.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clagsy.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clarge.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clarnd.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\claror.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clarot.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clatm1.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clatm2.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clatm3.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clatme.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clatmr.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\clatms.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlagge.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlagsy.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlaran.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlarge.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlarnd.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlaror.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlarot.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlatm1.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlatm2.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlatm3.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlatme.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlatmr.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\dlatms.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slagge.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slagsy.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slaran.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slarge.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slarnd.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slaror.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slarot.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slatm1.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slatm2.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slatm3.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slatme.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slatmr.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\slatms.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlagge.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlaghe.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlagsy.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlarge.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlarnd.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlaror.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlarot.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlatm1.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlatm2.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlatm3.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlatme.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlatmr.c
# End Source File
# Begin Source File

SOURCE=.\Matgen\zlatms.c
# End Source File
# End Group
# End Target
# End Project
