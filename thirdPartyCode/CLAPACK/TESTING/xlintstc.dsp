# Microsoft Developer Studio Project File - Name="xlintstc" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=xlintstc - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xlintstc.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xlintstc.mak" CFG="xlintstc - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "xlintstc - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "xlintstc - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "xlintstc - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Release\tmglib.lib ..\Release\clapack.lib ..\blas\Release\blas.lib ..\f2clibs\Release\libF77.lib ..\f2clibs\Release\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "xlintstc - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Debug\tmglib.lib ..\Debug\clapack.lib ..\blas\Debug\blas.lib ..\f2clibs\Debug\libF77.lib ..\f2clibs\Debug\libI77.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "xlintstc - Win32 Release"
# Name "xlintstc - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "alintst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Lin\aladhd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alaerh.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alaesm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alahd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alareq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alasum.c
# End Source File
# Begin Source File

SOURCE=.\Lin\alasvm.c
# End Source File
# Begin Source File

SOURCE=.\LIN\cchkq3.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ilaenv.c
# End Source File
# Begin Source File

SOURCE=.\Lin\xerbla.c
# End Source File
# Begin Source File

SOURCE=.\Lin\xlaenv.c
# End Source File
# End Group
# Begin Group "clintst"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Lin\cchkaa.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkeq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkgb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkhe.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkhp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchklq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkpb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkpp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkpt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkql.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkqp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkqr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchkrq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchksp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchksy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchktb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchktp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchktr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cchktz.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvgb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvhe.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvhp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvpb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvpp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvpt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cdrvsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrge.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrgt.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrhe.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrlq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrpo.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrql.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrqp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrqr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrrq.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrtr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrtz.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cerrvx.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgbt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgelqs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgeqls.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgeqrs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgerqs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cget01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cget02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cget03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cget04.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cget07.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgtt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgtt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cgtt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\chet01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\chkxer.c
# End Source File
# Begin Source File

SOURCE=.\Lin\chpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\claipd.c
# End Source File
# Begin Source File

SOURCE=.\Lin\claptm.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clarhs.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clatb4.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clatsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clatsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clattb.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clattp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clattr.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clavhe.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clavhp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clavsp.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clavsy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clqt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clqt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\clqt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cpbt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cpbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cpbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cpot01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cpot02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cpot03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cpot05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cppt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cppt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cppt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cppt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cptt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cptt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cptt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqlt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqlt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqlt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt11.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt12.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt13.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt14.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt15.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt16.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cqrt17.c
# End Source File
# Begin Source File

SOURCE=.\Lin\crqt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\crqt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\crqt03.c
# End Source File
# Begin Source File

SOURCE=.\LIN\crzt01.c
# End Source File
# Begin Source File

SOURCE=.\LIN\crzt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\csbmv.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cspt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cspt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\cspt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\csyt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\csyt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\csyt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctbt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctbt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctbt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctbt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctpt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctpt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctpt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctpt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctpt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctrt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctrt02.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctrt03.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctrt05.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctrt06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctzt01.c
# End Source File
# Begin Source File

SOURCE=.\Lin\ctzt02.c
# End Source File
# Begin Source File

SOURCE=.\LIN\icopy.c
# End Source File
# Begin Source File

SOURCE=.\Lin\sget06.c
# End Source File
# Begin Source File

SOURCE=.\Lin\slaord.c
# End Source File
# End Group
# End Group
# End Target
# End Project
