@echo off
:
: for 4NT only
:
set MODE=Release
:
: ======== SINGLE LIN TESTS ===========================

	@echo Data file for testing REAL LAPACK linear equation routines
	%MODE\xlintsts < stest.in >& stest.out 
:
: ======== COMPLEX LIN TESTS ==========================

	@echo Data file for testing COMPLEX LAPACK linear equation routines
	%MODE\xlintstc < ctest.in >& ctest.out 
:
: ======== DOUBLE LIN TESTS ===========================

	@echo Data file for testing DOUBLE PRECISION LAPACK linear equation routines
	%MODE\xlintstd < dtest.in >& dtest.out 
:
: ======== COMPLEX16 LIN TESTS ========================

	@echo Data file for testing COMPLEX16 LAPACK linear equation routines
	%MODE\xlintstz < ztest.in >& ztest.out
