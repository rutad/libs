@echo off
set MODE=Release
:
: Test the BLAS library. Results in the SUMM files 
:
	echo Deleting old SUMM files
	del /q *.SUMM
	echo Doing tests...
	xblat2s_%MODE\xblat2s < ..\sblat2.in
	xblat2d_%MODE\xblat2d < ..\dblat2.in
	xblat2c_%MODE\xblat2c < ..\cblat2.in
	xblat2z_%MODE\xblat2z < ..\zblat2.in
	xblat3s_%MODE\xblat3s < ..\sblat3.in
	xblat3d_%MODE\xblat3d < ..\dblat3.in
	xblat3c_%MODE\xblat3c < ..\cblat3.in
	xblat3z_%MODE\xblat3z < ..\zblat3.in
	echo Tests complete, see the new SUMM files
